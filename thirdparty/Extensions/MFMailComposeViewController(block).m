//
//  MFMailComposeViewController.m
//  MustacheBash
//
//  Created by Alexander Gavrilko on 07/02/15.
//  Copyright (c) 2015 Bright Newt. All rights reserved.
//

#import <objc/runtime.h>
#import "MFMailComposeViewController(block).h"

@interface MFMailComposeViewControllerBlockWrapper : NSObject <MFMailComposeViewControllerDelegate>

@property (copy, nonatomic) void(^handler)(MFMailComposeViewController *controller, MFMailComposeResult result, NSError *error);

@end


@implementation MFMailComposeViewControllerBlockWrapper

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if ( _handler )
    {
        _handler(controller, result, error);
    }
}

@end


@implementation MFMailComposeViewController(block)

- (void) setMailComposeControllerDidFinishWithResult:(void(^)(MFMailComposeViewController *controller, MFMailComposeResult result, NSError *error))block
{
    if ( block )
    {
        MFMailComposeViewControllerBlockWrapper *blockWrapper = [[MFMailComposeViewControllerBlockWrapper alloc] init];
        blockWrapper.handler = block;
        self.mailComposeDelegate = blockWrapper;

        objc_setAssociatedObject(self, "blockWrapper", blockWrapper, OBJC_ASSOCIATION_RETAIN);
    }
}

@end
