//
//  ViewController.swift
//  FaceShapeDetection
//
//  Created by Hassan Izhar on 11/10/17.
//  Copyright © 2017 Hassan Izhar. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import SwiftyStoreKit

extension CGRect {
    func scaled(to size: CGSize) -> CGRect {
        return CGRect(
            x: self.origin.x * size.width,
            y: self.origin.y * size.height,
            width: self.size.width * size.width,
            height: self.size.height * size.height
        )
    }
}

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, AVCapturePhotoCaptureDelegate{
    @IBOutlet weak var previewView: UIView!
    var captureSession: AVCaptureSession?
    var capturePhotoOutput: AVCapturePhotoOutput?
    let shapeLayer = CAShapeLayer()
    let faceDetection = VNDetectFaceRectanglesRequest()
    let faceLandmarks = VNDetectFaceLandmarksRequest()
    let faceLandmarksDetectionRequest = VNSequenceRequestHandler()
    let faceDetectionRequest = VNSequenceRequestHandler()
    var outputImage: UIImage!
    var middleLineLayer = CAShapeLayer()
    var upperEyeLineLayer = CAShapeLayer()
    var jawLineLayer = CAShapeLayer()
    var middleLineFrame = CGRect.zero
    var upperEyeLineFrame = CGRect.zero
    var jawLineFrame = CGRect.zero
    var shape = ""
    let threshold: CGFloat = 0.0
    var faceDetected = false
    
    var originalImage: UIImage!

    
    
    lazy var previewLayer: AVCaptureVideoPreviewLayer? = {
        guard let session = self.captureSession else { return nil }
        
        var previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        return previewLayer
    }()
    
    var frontCamera: AVCaptureDevice? = {
        return AVCaptureDevice.defaultDevice(withDeviceType: AVCaptureDevice.DeviceType.builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front)
        
        
        //AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for  mediaType: AVMediaType.video, position: .front)
    }()
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSession()
        //captureSession?.startRunning()
        //restorePreviousPurchases()
    }
    
    func restorePreviousPurchases(){
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            if results.restoreFailedPurchases.count > 0 {
                print("Restore Failed: \(results.restoreFailedPurchases)")
            }
            else if results.restoredPurchases.count > 0 {
                for purchase in results.restoredPurchases{
                    if purchase.productId == "com.look.findfaceshape.removeads"{
                        if !Util.getPurchasedFlag(){
                            Util.setPurchasedFlag(flag: true)
                        }
                        UserDefaults.standard.synchronize()
                    }
                }
            }
            else {
                print("Nothing to Restore")
            }
        }
    }
    
    func setupSession(){
        captureSession = AVCaptureSession()
        guard let session = captureSession, let captureDevice = frontCamera else { return }
        do {
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            session.beginConfiguration()
            if session.canAddInput(deviceInput) {
                session.addInput(deviceInput)
            }
            // Get an instance of ACCapturePhotoOutput class
            capturePhotoOutput = AVCapturePhotoOutput()
            capturePhotoOutput?.isHighResolutionCaptureEnabled = true
            // Set the output on the capture session
            session.addOutput(capturePhotoOutput!)
            let output = AVCaptureVideoDataOutput()
            //output.videoSettings = [kCVPixelBufferPixelFormatTypeKey: Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange), AVVideoWidthKey : 100, AVVideoHeightKey: 100]

            output.videoSettings = [
                String(kCVPixelBufferPixelFormatTypeKey) : Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange), AVVideoWidthKey : 100, AVVideoHeightKey: 100]
            output.alwaysDiscardsLateVideoFrames = true
            if session.canAddOutput(output) {
                session.addOutput(output)
            }
            session.commitConfiguration()
            let queue = DispatchQueue(label: "output.queue")
            output.setSampleBufferDelegate(self, queue: queue)
            print("setup delegate")
        } catch {
            print("can't setup session")
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        shapeLayer.frame = view.frame
        previewLayer?.frame = view.frame
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //shapeLayer.frame = view.frame

        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 2.0
        //needs to filp coordinate system for Vision
        shapeLayer.setAffineTransform(CGAffineTransform(scaleX: -1, y: -1))
        guard let previewLayer = previewLayer else { return }
        previewView.layer.addSublayer(previewLayer)
        previewView.layer.addSublayer(shapeLayer)
        captureSession?.startRunning()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        captureSession?.stopRunning()

    }
    
    @IBAction func showInfo(_ sender: Any) {
        self.performSegue(withIdentifier: "showPopup", sender: self)
        //self.performSegue(withIdentifier: "showOutput", sender: self)

    }
    
    @IBAction func takePhoto(_ sender: Any) {
        if !faceDetected{
            let alert = UIAlertController(title: "Alert", message: "No face is detected", preferredStyle: UIAlertControllerStyle.alert)
            let alertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
            return
        }
        if middleLineFrame.size == CGSize.zero && upperEyeLineFrame.size == CGSize.zero && jawLineFrame.size == CGSize.zero{
            let alert = UIAlertController(title: "Alert", message: "No lines are drawn on face", preferredStyle: UIAlertControllerStyle.alert)
            let alertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if middleLineFrame.maxY / self.view.bounds.size.height < 0.40{
            let alert = UIAlertController(title: "Alert", message: "Show your full face", preferredStyle: UIAlertControllerStyle.alert)
            let alertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
            return
        }
        guard let capturePhotoOutput = self.capturePhotoOutput else { return }
        
        // Get an instance of AVCapturePhotoSettings class
        let photoSettings = AVCapturePhotoSettings()
        
        // Set photo settings for our need
        photoSettings.isAutoStillImageStabilizationEnabled = true
        photoSettings.isHighResolutionPhotoEnabled = true
       // photoSettings.flashMode = .auto
        
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            photoSettings.flashMode = .off
        }else{
            photoSettings.flashMode = .auto
        }
        photoSettings.flashMode = .off

        // Call capturePhoto method by passing our photo settings and a delegate implementing AVCapturePhotoCaptureDelegate
        capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self)
    }
    
    
    
    func captureOutput(_ output: AVCaptureOutput, didOutputSampleBuffer sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        let attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate)
        let ciImage = CIImage(cvImageBuffer: pixelBuffer!, options: attachments as! [String : Any]?)
        //leftMirrored for front camera
        let ciImageWithOrientation = ciImage.applyingOrientation(Int32(UIImageOrientation.leftMirrored.rawValue))
        detectFace(on: ciImageWithOrientation)
    }
    
    
    
    func detectFace(on image: CIImage) {
        try? faceDetectionRequest.perform([faceDetection], on: image)
        if let results = faceDetection.results as? [VNFaceObservation] {
            if !results.isEmpty {
                faceDetected = true
                faceLandmarks.inputFaceObservations = results
                detectLandmarks(on: image)
            }else{
                faceDetected = false
                jawLineFrame = CGRect.zero
                middleLineFrame = CGRect.zero
                upperEyeLineFrame = CGRect.zero
                DispatchQueue.main.async {
                    self.shapeLayer.sublayers?.removeAll()
                }
            }
        }
    }
    
    
    
    func detectLandmarks(on image: CIImage) {
        try? faceLandmarksDetectionRequest.perform([faceLandmarks], on: image)
        if let landmarksResults = faceLandmarks.results as? [VNFaceObservation] {
            for observation in landmarksResults {
                DispatchQueue.main.async {
                    if let boundingBox = self.faceLandmarks.inputFaceObservations?.first?.boundingBox {
                        let faceBoundingBox = boundingBox.scaled(to: self.view.bounds.size)
                        DispatchQueue.main.async {
                            
                            
                            //Face contour
                            let faceContour = observation.landmarks?.faceContour
                            
                            let faceContourPoints = faceContour?.normalizedPoints.map { (point: CGPoint) -> (x: CGFloat, y: CGFloat) in
                                let pointX = point.x * faceBoundingBox.width + faceBoundingBox.origin.x
                                let pointY = point.y * faceBoundingBox.height + faceBoundingBox.origin.y
                                return (x: pointX, y: pointY)
                            }
                            let foreheadHeight = boundingBox.height + (boundingBox.height / 5)
                            //the middle line
                            let size = CGSize(width: 2,
                                              height: foreheadHeight * self.view.bounds.height)
                            let origin = CGPoint(x: boundingBox.midX * self.view.bounds.width,
                                                 y: boundingBox.minY * self.view.bounds.height)
                            
                            self.middleLineLayer.frame = CGRect(origin: origin, size: size)
                            self.middleLineLayer.borderColor = UIColor.green.cgColor
                            self.middleLineLayer.borderWidth = 2
                            self.shapeLayer.addSublayer(self.middleLineLayer)
                            self.middleLineFrame = self.middleLineLayer.frame
                            
                        }
                    }
                }
            }
        }
    }
    
    func convertPointsForFace(_ landmark: VNFaceLandmarkRegion2D?, _ boundingBox: CGRect) {
        
        if let points = landmark?.normalizedPoints {
            let faceLandmarkPoints = points.map { (point: CGPoint) -> (x: CGFloat, y: CGFloat) in
                let pointX = point.x * boundingBox.width + boundingBox.origin.x
                let pointY = point.y * boundingBox.height + boundingBox.origin.y
                return (x: pointX, y: pointY)
            }
            
            DispatchQueue.main.async {
                self.draw(points: faceLandmarkPoints)
            }
        }
    }
    
    func draw(points: [(x: CGFloat, y: CGFloat)]) {
        let newLayer = CAShapeLayer()
        newLayer.strokeColor = UIColor.red.cgColor
        newLayer.lineWidth = 2.0
        let path = UIBezierPath()
        path.move(to: CGPoint(x: points[0].x, y: points[0].y))
        for i in 0..<points.count - 1 {
            let point = CGPoint(x: points[i].x, y: points[i].y)
            path.addLine(to: point)
            path.move(to: point)
        }
        path.addLine(to: CGPoint(x: points[0].x, y: points[0].y))
        newLayer.path = path.cgPath
        
        shapeLayer.addSublayer(newLayer)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Capture Output delegates
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        let imageData = photo.fileDataRepresentation()
        let capturedImage = UIImage.init(data: imageData! , scale: 1.0)
        let invertedImage = UIImage(cgImage: (capturedImage?.cgImage)!, scale: (capturedImage?.scale)!, orientation: UIImageOrientation.leftMirrored)
        // Save our captured image to photos album if needed
        let height = self.view.frame.height - previewView.frame.minY
//        self.outputImage = cropToBounds(image: invertedImage, width: Double(previewView.frame.width), height: Double(height))
       
        
        //let tempImageView = UIImageView(frame: previewView.frame)
        
        
        
        let constantValue = invertedImage.size.width / previewView.frame.size.width
        
        
        print("Hello there00000 \(previewView.frame)")
        let percentPreview = (previewView.frame.size.height+previewView.frame.origin.y) / previewView.frame.size.height
        print("percentPreview \(percentPreview)")

        
        
        //let tempImageView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: previewView.frame.size.width, height: previewView.frame.size.width))
        
        
        let tempImageView = UIImageView(frame: previewView.frame)
        
        
        
        //tempImageView.image = cropToBounds(image: invertedImage, width: Double(previewView.frame.width), height: Double(height - (constantValue * 190)))
        tempImageView.image = invertedImage

        
        
        
//        shapeLayer.transform = CATransform3DMakeScale(shapeLayer.frame.width/32, shapeLayer.frame.height/32, 0)
        //shapeLayer.frame = tempImageView.bounds
        print("Hello there \(shapeLayer.frame)")
        
        //shapeLayer.frame = CGRect.init(x: 0, y: 0, width: tempImageView.frame.size.width, height: tempImageView.frame.size.width)
        print("Hello there2 \(shapeLayer.frame)")

        
        
        tempImageView.layer.addSublayer(shapeLayer)
        tempImageView.layoutIfNeeded()
        UIGraphicsBeginImageContextWithOptions(tempImageView.bounds.size, false, 0);
        tempImageView.drawHierarchy(in: tempImageView.bounds, afterScreenUpdates: true)
        let copied = UIGraphicsGetImageFromCurrentImageContext();
        
        self.outputImage = cropFromBottom(image: copied!, percentPreview: percentPreview)
        
        //self.outputImage = copied

        //self.outputImage = cropToBounds(image: invertedImage, width: Double(previewView.frame.width), height: Double(height - (constantValue * 190)))
        
        self.originalImage = cropToBounds(image: invertedImage, width: Double(previewView.frame.width), height: Double(height - (constantValue * 190)), percentPreview: percentPreview)
        
        UIGraphicsEndImageContext();
        
        
        detectFaceType()
    }
    
    func detectFaceType(){
        let L4 = middleLineFrame.height
        
        if (L4/2) > L4{
            if L4 > L4 {
                shape = "Rectangle"
            }
        }else{
            
        }
        self.performSegue(withIdentifier: "showOutput", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let outputViewController = segue.destination as? OutputViewController{
            if self.outputImage == nil {
                self.outputImage = UIImage(named: "splash")
                self.originalImage = UIImage(named: "splash")
            }
            
            
            outputViewController.image = self.outputImage
            outputViewController.originalImage = self.originalImage

            outputViewController.shape = self.shape
            outputViewController.shapeLayerFrame = self.shapeLayer.frame
        }
    }
    
    func cropToBounds(image: UIImage, width: Double, height: Double, percentPreview: CGFloat) -> UIImage {
        
        let contextImage: UIImage = UIImage(cgImage: image.cgImage!)
        
        let contextSize: CGSize = contextImage.size
        print("contextSize \(contextSize)")

        
        
       
        
        
        
        let posX: CGFloat = 0.0
        let cgwidth: CGFloat = contextSize.width * percentPreview
        let posY: CGFloat = contextSize.width - cgwidth

        let cgheight: CGFloat = cgwidth

        
        let rect: CGRect = CGRect(x: posY, y: posX, width: cgwidth, height: cgheight)
        
        
        print("rect \(rect)")

        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = (contextImage.cgImage?.cropping(to: rect))!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    func cropFromBottom(image: UIImage, percentPreview: CGFloat) -> UIImage {
        
        let contextImage: UIImage = UIImage(cgImage: image.cgImage!)
        
        let contextSize: CGSize = contextImage.size
        print("The new contextSize  is \(contextSize)")

        
        let posX: CGFloat = 0.0
        let cgwidth: CGFloat = contextSize.width
        let cgheight: CGFloat = contextSize.height * percentPreview

        let posY: CGFloat = contextSize.height - cgheight
        
        
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        
        print("The new image Rect is \(rect)")
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = (contextImage.cgImage?.cropping(to: rect))!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
}

