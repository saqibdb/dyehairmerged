//
//  ALInterstitialAd(scheduling).h
//  MustacheBash
//
//  Created by Alexander Gavrilko on 21/02/15.
//  Copyright (c) 2015 Bright Newt. All rights reserved.
//

#import <ALInterstitialAd.h>

@interface ALInterstitialAd(scheduling)

/*!
 Show advertisement if it's available right now. If advertisement won't be loaded during given time than nothing will hapen.
 
 @param timeInterval
 Interval for waiting for loading. If value is negative than current scheduled advertisement will be cancelled and noting will be showed.
 
 @result
 YES if advertisement was shosed immediately.
 */
+ (BOOL) scheduleAdvertisementForTime:(NSTimeInterval)timeInterval;


/*!
 Cancel any scheduled advertisement presentations.
 */
+ (void) unscheduleCurrentAdvertisement;

@end
