//
//  CustomFriendsTableViewCell.h
//  YourPerfectHairstyle32
//
//  Created by Qaiser Abbas on 6/29/17.
//  Copyright © 2017 APPreciate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface CustomFriendsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet AsyncImageView *userImage;

@end
