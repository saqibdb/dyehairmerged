//
//  ApplicationRater.h
//  MustacheBash
//
//  Created by Alexander Gavrilko on 06/02/15.
//  Copyright (c) 2015 Bright Newt. All rights reserved.
//

/*!
 Use this array to control review propositons.
 */
#define APPLICATION_RATER_FIRE_NUMBER   2 , 3, 5


/*!
 Application rater class, added instead of iRate on 6 Febraury 2015.
 */
@interface ApplicationRater : NSObject

/*!
 Create application rater instance to handle review logic.
 
 @param agreementHandler
 Block which will be colled when user will decide to review the application.
 
 @result
 YES if rater was created.
 */
+ (BOOL) awakeWithAgreementHandler:(void(^)())agreementHandler;
+(void)showFirstTime;

@end
