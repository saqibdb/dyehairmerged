//
//  UIAlertView(block).m
//  Capsule
//
//  Created by Gavrilko Alexander on 13.08.14.
//  Copyright (c) 2014 CodeIT. All rights reserved.
//

#import <objc/runtime.h>
#import "UIAlertView(block).h"


@interface UIAlertViewBlockWrapper : NSObject <UIAlertViewDelegate>

@property (readonly, nonatomic) NSMutableDictionary *handlers;

@end


@implementation UIAlertViewBlockWrapper

- (id) init
{
    self = [super init];
    
    if ( self )
    {
        _handlers = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    void(^block)() = _handlers[[alertView buttonTitleAtIndex:buttonIndex]];
    
    if ( block )
    {
        block();
    }
}


@end



@implementation UIAlertView(block)

- (id) initWithTitle:(NSString *)title message:(NSString *)message buttonTitlesAndHandlers:(NSString *)key, ...
{
    self = [super init];
    
    if ( self )
    {
        va_list args;
        va_start(args, key);
        

        
        UIAlertViewBlockWrapper *blockWrapper = [[UIAlertViewBlockWrapper alloc] init];
        
        objc_setAssociatedObject(self, "UIAlertViewBlockWrapper", blockWrapper, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        
                                                 
        self.title = title;
        self.message = message;
        self.delegate = blockWrapper;
        
        
        while ( key )
        {
            id handler = va_arg(args, id);
            
            if ( handler )
            {
                [blockWrapper.handlers setObject:[handler copy]
                                          forKey:key];
            }
            
            
            [self addButtonWithTitle:key];
            
            key = va_arg(args, NSString*);
        }
        
        
        va_end(args);
    }
    
    return self;
}


@end
