//
//  PurchasePopupVC.h
//  Change Your Hair 2
//
//  Created by Sreekanthb on 03/05/2016.
//  Copyright © 2016 APPreciate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMPack.h"

@protocol PurchasePopupDelegate <NSObject>

-(void)removeBanner;
-(void)unlockPack:(DMPack*)pack;
-(void)unlockEverything;

@end

@interface PurchasePopupVC : UIViewController

-(IBAction)removeBanner;
-(IBAction)unlockPack;
-(IBAction)unlockEverything;
-(IBAction)dismissSelf;
@property(nonatomic, retain)    NSString*     packName;

@property(nonatomic, retain)    IBOutlet    UIImageView* bgImage;
@property(nonatomic, retain)    id<PurchasePopupDelegate>     delegate;
@end
