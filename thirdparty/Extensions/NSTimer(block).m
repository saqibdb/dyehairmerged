//
//  NSTimer(block).m
//  Capsule
//
//  Created by Gavrilko Alexander on 21.07.14.
//  Copyright (c) 2014 CodeIT. All rights reserved.
//

#import <objc/runtime.h>
#import "NSTimer(block).h"

@implementation NSTimer(block)

+ (NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)ti block:(void (^)(NSTimer *))block userInfo:(id)userInfo repeats:(BOOL)yesOrNo
{
    NSTimer *timer= [NSTimer scheduledTimerWithTimeInterval:ti
                                                     target:self
                                                   selector:@selector(_block_timerFired:)
                                                   userInfo:userInfo
                                                    repeats:yesOrNo];

    if ( block )
    {
        objc_setAssociatedObject(timer, "block", [block copy], OBJC_ASSOCIATION_RETAIN);
    }
    
    return timer;
}


#pragma mark - private

+ (void) _block_timerFired:(NSTimer*)sender
{
    void (^block)(NSTimer *sender) = objc_getAssociatedObject(sender, "block");
    if ( block )
    {
        block(sender);
    }
}


@end
