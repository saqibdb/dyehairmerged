//
//  NSTimer(block).h
//  Capsule
//
//  Created by Gavrilko Alexander on 21.07.14.
//  Copyright (c) 2014 CodeIT. All rights reserved.
//

@interface NSTimer(block)

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)ti block:(void(^)(NSTimer *sender))block userInfo:(id)userInfo repeats:(BOOL)yesOrNo;

@end
