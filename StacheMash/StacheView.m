//
//  StacheView.m
//  StacheMash
//
//  Created by Konstantin Sokolinskyi on 1/19/12.
//  Copyright (c) 2012 Bright Newt. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "StacheView.h"
#import "GUIHelper.h"

static const CGFloat kMinActivationAngle = 0.03f;
static const CGFloat kMinActivationScale = 7.0f;

@interface StacheView ()

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImage *unColoredImage;  //duc.tt
@property (strong, nonatomic) UIImage *coloredImage;  //duc.tt
//@property (strong, nonatomic) UIImage *coloredResizedImage;  //duc.tt

@property (strong, nonatomic) NSArray *imagesArray;
@property (assign, nonatomic) NSInteger currentStacheImageIndex;

@property (strong, nonatomic) UIPinchGestureRecognizer *pinchGesture;
@property (strong, nonatomic) UIRotationGestureRecognizer *rotationGesture;

@property (strong, nonatomic) UIView *handlesView;


- (void)setActive;
- (void)setInactive;

- (CGRect)handlesFrameForRect: (CGRect)frame;
- (void)handleTap: (UITapGestureRecognizer*)gestureRecognizer;

@end


@implementation StacheView
{
    enum {
        Unknown,
        Scale,
        Rotation,
    } _type;
    CGFloat _activatedAngle;
    CGFloat _activatedScale;
    NSMutableArray *_touches;
    
    CGSize _minSize;
    CGSize _maxSize;

}

@synthesize delegate = __delegate;
@synthesize hasMultipleColors = __hasMultipleColors;

@synthesize imageView = _imageView;
@synthesize imageColor = _imageColor;   //duc.tt
@synthesize imagesArray = _imagesArray;
@synthesize currentStacheImageIndex = _currentStacheImageIndex;
@synthesize pinchGesture = _pinchGesture;
@synthesize rotationGesture = _rotationGesture;
@synthesize enabled = _enabled;
@synthesize handlesView = _handlesView;
@synthesize stache = _stache;


#pragma mark - @property (nonatomic, assign) BOOL enabled;

- (void)setEnabled: (BOOL)enabled
{
    if ( enabled == _enabled ) {
        return;
    }
    else {
        _enabled = enabled;
        
        if (self.enabled) {
            [self setActive];
        }
        else {
            [self setInactive];
        }
    }
}


- (void)setActive
{
   // [self.pinchGesture setEnabled: YES];
   // [self.rotationGesture setEnabled: YES];
    
    [self addSubview: self.handlesView];
    [self sendSubviewToBack: self.handlesView];
}


- (void)setInactive
{
   // [self.pinchGesture setEnabled: NO];
   // [self.rotationGesture setEnabled: NO];
    
    [self.handlesView removeFromSuperview];
}


- (BOOL)enabled
{
    return _enabled;
}


#pragma mark - View Setup

- (id)initWithFrame: (CGRect)frame imagesArray: (NSArray*)imagesArray;
{
    if ( 0 == [imagesArray count] ) {
        error(@"empty imagesArray supplied");
        return nil;
    }
    
    self = [super initWithFrame: frame];
    if ( self ) {
        
        _minSize = CGSizeMake(frame.size.width * 0.3f,
                              frame.size.height * 0.3f);
        _maxSize = CGSizeMake(frame.size.width * 2.0f,
                              frame.size.height * 2.0f);
        
        self.imagesArray = imagesArray;
        self.currentStacheImageIndex = 0;
        __hasMultipleColors = (1 < [self.imagesArray count]);
                
        UIImage *image = [self.imagesArray objectAtIndex: self.currentStacheImageIndex];
        self.imageView = [[UIImageView alloc] initWithFrame: frame];
        //self.imageView.center = CGPointMake(0.5 * frame.size.width, 0.5 * frame.size.height);
        //self.imageView.image = image  //duc.tt    Comment this line, use the line below
        [self setImage:image];  //duc.tt
        //self.imageView.backgroundColor = [UIColor magentaColor];
        self.imageView.contentMode = UIViewContentModeScaleToFill;
        [self addSubview: self.imageView];
        
        // add TAP gesture recognizer
        UITapGestureRecognizer *tapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget: self
                                                action: @selector(handleTap:)];
        [self addGestureRecognizer: tapGesture];
        
        self.handlesView = [[UIView alloc] initWithFrame: [self handlesFrameForRect: frame]];
        self.handlesView.center = self.imageView.center;
        self.handlesView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent: 0.05];
        self.handlesView.layer.borderWidth = 1.0;
        self.handlesView.layer.borderColor = [UIColor colorWithWhite: 0.9 alpha: 0.6].CGColor;
        self.handlesView.layer.cornerRadius = (100 < image.size.width ? 15.0 : 5.0);
        self.handlesView.userInteractionEnabled = NO;
        
        self.multipleTouchEnabled = YES;
    }
    
    return self;
}


- (BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if ( _touches.count )
    {
        return YES;
    }
    
    return CGRectContainsPoint([self _activeFrame], point);
}

- (UIView*) hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if ( _touches.count )
    {
        return self;
    }
    
    if ( CGRectContainsPoint([self _activeFrame], point) )
    {
        return self;
    }
    
    return nil;
}


- (CGRect)handlesFrameForRect: (CGRect)frame
{
    CGFloat handlesScaleFactor = 1.2;
    CGSize size = CGSizeMake(frame.size.width * handlesScaleFactor, frame.size.height * handlesScaleFactor);
    CGPoint origin = CGPointMake((self.bounds.size.width - size.width) * 0.5f,
                                 (self.bounds.size.height - size.height) * 0.5f);
    return  (CGRect){origin, size};
}

- (void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.imageView.frame = self.bounds;
    self.handlesView.frame = [self handlesFrameForRect:self.bounds];
    [self.imageView setNeedsDisplay];
}


- (void)setImage: (UIImage*)newImage
{
    if ( nil == newImage ) {
        error(@"nil image supplied");
        return;
    }
    
    self.unColoredImage = newImage;
    [self setNewColoredImage:newImage]; //duc.tt
    self.imageSaturation = 0.5; //duc.tt
    self.imageBrightness = 0.5; //duc.tt
    self.imageContrast = 0.5; //duc.tt
    self.imageView.image = newImage;
}

//duc.tt
- (void)setNewColoredImage:(UIImage*)newImage
{
    self.coloredImage = newImage; //duc.tt
    
    //duc.tt resize images 3 times scale down
//    CGRect newRect = CGRectMake(0, 0, newImage.size.width/1.0, newImage.size.width/1.0);
//    UIGraphicsBeginImageContext(newRect.size);
//    [newImage drawInRect:newRect];
//    self.coloredResizedImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
}

//duc.tt
- (float)calculatedSaturation
{
    // always return in [0 -> 2]
    return 2*self.imageSaturation;
}

//duc.tt
- (float)calculatedBrightness
{
    return 0.15*(self.imageBrightness - 0.5);
}

//duc.tt
- (float)calculatedContrast
{
    if (self.imageContrast >= 0.5) {
        return 1 + 0.5*(self.imageContrast - 0.5);
    } else {
        return 1 + 0.25*(self.imageContrast - 0.5);
    }
}

//duc.tt
- (int)displaySaturation
{
    return 100*[self calculatedSaturation];
}

//duc.tt
- (int)displayBrightness
{
    return 2000*[self calculatedBrightness];
}

//duc.tt
- (int)displayContrast
{
    return ([self calculatedContrast] -1)*400;
}

//duc.tt
- (void)setImageColor:(UIColor *)color
{
    _imageColor = [color copy];
    self.imageView.image = [self imageWithImage:self.unColoredImage adjustNewColor:self.imageColor];
}

//duc.tt
- (void)refreshImageSettings:(BOOL)inLoop
{
    static BOOL inProgress = NO;
//    if (inLoop) {
//        if (!inProgress) {
//            inProgress = YES;
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                UIImage *newImage = [self imageWithImage:self.coloredResizedImage adjustNewColor:nil];;
//                [self.imageView performSelectorOnMainThread:@selector(setImage:) withObject:newImage waitUntilDone:NO];
//                inProgress = NO;
//            });
//        }
//    } else {
    if (!(inLoop && inProgress)) {
        inProgress = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *newImage = [self imageWithImage:self.coloredImage adjustNewColor:nil];;
            [self.imageView performSelectorOnMainThread:@selector(setImage:) withObject:newImage waitUntilDone:NO];
            inProgress = NO;
        });
    }
}

- (void)setNewStacheImageArray: (NSArray*)imagesArray
{
    if ( 0 == [imagesArray count] ) {
        error(@"empty imagesArray provided");
        return;
    }
    
    self.imagesArray = imagesArray;
    self.currentStacheImageIndex = 0;
    
    __hasMultipleColors = (1 < [self.imagesArray count]);
    
    UIImage *image = [imagesArray objectAtIndex: self.currentStacheImageIndex];
    // Sun - ipad support
    CGFloat widthImage, heightImage;
    //Sun - iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        if ([GUIHelper isIPadretina]){//iPad retina
            widthImage = 2*image.size.width;
            heightImage = 2*image.size.height;
        }
        else{
            widthImage = 1.3*image.size.width;
            heightImage = 1.3*image.size.height;
        }
    }
    else if ( [GUIHelper isPhone5] ) {
        widthImage = image.size.width;
        heightImage = image.size.height;
    }
    else {
        widthImage = image.size.width/2;
        heightImage = image.size.height/2;
    }

    
//    [self setNewStacheImage: image withFrame: CGRectMake(0, 0,
//                                                         0.5 * image.size.width,
//                                                         0.5 * image.size.height)];
    
    [self setNewStacheImage: image withFrame: CGRectMake(0, 0,
                                                         widthImage,
                                                         heightImage)];

    if (self.enabled) {
        [self setActive];
    }
}


- (void)setNewStacheImage: (UIImage*)newImage withFrame: (CGRect)frame
{
    [self.imageView removeFromSuperview];
    self.imageView = nil;
    
//    CGFloat scaleFactor;
//    if ( 0 != self.actualScale ) {
//        scaleFactor = 1.0 / self.actualScale;
//    }
//    else {
//        error(@"self.actualScale == 0!!!");
//        scaleFactor = 1.0;
//    }
    
    CGPoint oldCenter = self.center;
    CGRect realFrame = frame;

    self.frame = realFrame;
    CGRect bounds = CGRectZero;
    bounds.size = realFrame.size;
    self.bounds = bounds;
    
    self.imageView = [[UIImageView alloc] initWithFrame: frame];
    self.imageView.center = CGPointMake(0.5 * self.bounds.size.width,
                                        0.5 * self.bounds.size.height);
    //self.imageView.image = image  //duc.tt    Comment this line, use the line below
    [self setImage:newImage];   //duc.tt
    [self addSubview: self.imageView];
    
    self.handlesView.frame = [self handlesFrameForRect: frame];
    self.handlesView.center = self.imageView.center;
    
    self.center = oldCenter;
}

//duc.tt
- (void) scaleUp:(CGFloat)scale
{
    [self scaleUpXY:CGPointMake(scale, scale)];
}

- (void) scaleUpXY:(CGPoint)scale
{
    CGAffineTransform transform = self.transform;
    self.transform = CGAffineTransformIdentity;
    
    CGRect frame = self.frame;
    CGSize size = CGSizeMake(frame.size.width * scale.x, frame.size.height * scale.y);
    
    frame.origin.x -= (frame.size.width - size.width) * 0.5f;
    frame.origin.y -= (frame.size.height - size.height) * 0.5f;
    frame.size = size;
    
    frame = [self _boundFrame:frame];
    
    self.frame = frame;
    self.transform = transform;
}

- (UIImage*)image
{
    return  self.imageView.image;
}


#pragma mark - Actions


- (void)nextStacheColor
{
    if ( self.currentStacheImageIndex < [self.imagesArray count] - 1) {
        self.currentStacheImageIndex++;
    }
    else {
        self.currentStacheImageIndex = 0;
    }
    
    [self setImage: [self.imagesArray objectAtIndex: self.currentStacheImageIndex]];
}

- (void)setColorWithIndex: (NSUInteger)index
{
    if ( index < [self colorCount] ) {
        [self setImage: [self.imagesArray objectAtIndex: index]];
    }
    else {
        [self setImage: [self.imagesArray objectAtIndex: 0]];
    }
}

- (NSUInteger)colorCount
{
    return [self.imagesArray count];
}

#pragma mark - Private

- (void)handleTap: (UITapGestureRecognizer*)gestureRecognizer
{
    if ( gestureRecognizer.state == UIGestureRecognizerStateEnded ) {
        [self.delegate stacheViewTapped: self];
    }
}


#pragma mark - touches

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ( !self.enabled )
    {
        return;
    }
    
    for ( UITouch *touch in touches )
    {
        if ( !_touches )
        {
            _touches = [[NSMutableArray alloc] initWithObjects:touch, nil];
        }
        else if ( ![_touches containsObject:touch] )
        {
            [_touches addObject:touch];
        }
    }
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //duc.tt button Rotate mode
    if (self.touchMode == StacheViewTouchModeRotate) {
        if (_touches.count == 1) {
            UITouch *touch = _touches[0];
            CGPoint previousLocation = [touch previousLocationInView:self.superview];
            CGPoint location = [touch locationInView:self.superview];
            
            self.center = CGPointMake(self.center.x + location.x - previousLocation.x, self.center.y + location.y - previousLocation.y);
        }
        else if (_touches.count >= 2) {
            UITouch *touch1 = _touches[0];
            UITouch *touch2 = _touches[1];
            
            CGPoint previousLocation1 = [touch1 previousLocationInView:self];
            CGPoint previousLocation2 = [touch2 previousLocationInView:self];
            CGPoint location1 = [touch1 locationInView:self];
            CGPoint location2 = [touch2 locationInView:self];
            CGPoint previousCenter = self.center;

            CGPoint previousDisp = CGPointMake(previousLocation1.x - previousLocation2.x, previousLocation1.y - previousLocation2.y);
            CGPoint disp = CGPointMake(location1.x - location2.x, location1.y - location2.y);
            
            CGFloat previousLen = sqrtf(previousDisp.x * previousDisp.x + previousDisp.y * previousDisp.y);
            CGFloat len = sqrtf(disp.x * disp.x + disp.y * disp.y);
            
            CGFloat rotation = 0.0f;
            

            if ( previousLen && len )
            {
                CGPoint previousVec = CGPointMake(previousDisp.x / previousLen, previousDisp.y / previousLen);
                CGPoint vec = CGPointMake(disp.x / len, disp.y / len);
                
                CGFloat previousAngle = atan2f(previousVec.x, previousVec.y);
                CGFloat angle = atan2f(vec.x, vec.y);
                
                rotation = angle - previousAngle;
            }
            
            self.transform = CGAffineTransformRotate(self.transform, -rotation);
            self.center = previousCenter;
        }
        
        return;
    }
    
    //duc.tt button Zoom mode
    if (self.touchMode == StacheViewTouchModeZoom) {
        if (_touches.count >= 1) {
            UITouch *touch = _touches[0];
            CGPoint previousLocation = [touch previousLocationInView:self.superview];
            CGPoint location = [touch locationInView:self.superview];
            CGPoint previousCenter = self.center;
            
            float transformRotation = atan2f(self.transform.b, self.transform.a);
            CGFloat previousDist = [self distanceBetweenPoint:previousLocation andPoint:previousCenter];
            float previousAngle = atan2f(previousLocation.x - previousCenter.x, previousLocation.y - previousCenter.y);
            CGFloat dist = [self distanceBetweenPoint:location andPoint:self.center];
            float currentAngle = atan2f(location.x - self.center.x, location.y - self.center.y);
            
            float scaleX = ABS(dist*sin(currentAngle+transformRotation))/ABS(previousDist*sin(previousAngle+transformRotation));
            float scaleY = ABS(dist*cos(currentAngle+transformRotation))/ABS(previousDist*cos(previousAngle+transformRotation));
            scaleX = MAX(MIN(scaleX, 1.02), 0.98);    //Limit epsilon
            scaleY = MAX(MIN(scaleY, 1.02), 0.98);    //Limit epsilon
            
//            //keep zoom direction (zoom in or zoom out)
//            if ((self.previousZoomFactor.x > 1 && scaleX < 1)
//                || (self.previousZoomFactor.x < 1 && scaleX > 1))
//            {
//                scaleX = 1/scaleX;
//            }
//            
//            if ((self.previousZoomFactor.y > 1 && scaleY < 1)
//                || (self.previousZoomFactor.y < 1 && scaleY > 1))
//            {
//                scaleY = 1/scaleY;
//            }
//            
//            self.previousZoomFactor = CGPointMake(scaleX, scaleY);
            
            [self scaleUpXY:CGPointMake(scaleX, scaleY)];
            
            self.center = previousCenter;
        }
        
        return;
    }
    
    switch ( _touches.count )
    {
        case 1:
        {
            UITouch *touch = _touches[0];
            CGPoint previousLocation = [touch previousLocationInView:self.superview];
            CGPoint location = [touch locationInView:self.superview];
            
            CGPoint disp = CGPointMake(location.x - previousLocation.x, location.y - previousLocation.y);
            CGPoint center = CGPointMake(self.center.x + disp.x, self.center.y + disp.y);
            
            self.center = center;
            break;
        }
            
        case 2:
        {
            UITouch *touch1 = _touches[0];
            UITouch *touch2 = _touches[1];
            
            CGPoint previousLocation1 = [touch1 previousLocationInView:self] ;
            CGPoint previousLocation2 = [touch2 previousLocationInView:self];
            CGPoint location1 = [touch1 locationInView:self];
            CGPoint location2 = [touch2 locationInView:self];
            
            
            CGPoint previousDisp = CGPointMake(previousLocation1.x - previousLocation2.x, previousLocation1.y - previousLocation2.y);
            CGPoint disp = CGPointMake(location1.x - location2.x, location1.y - location2.y);
            
            CGFloat previousLen = sqrtf(previousDisp.x * previousDisp.x + previousDisp.y * previousDisp.y);
            CGFloat len = sqrtf(disp.x * disp.x + disp.y * disp.y);
            
            CGFloat rotation = 0.0f;
            
            
            if ( previousLen && len )
            {
                CGPoint previousVec = CGPointMake(previousDisp.x / previousLen, previousDisp.y / previousLen);
                CGPoint vec = CGPointMake(disp.x / len, disp.y / len);
                
                CGFloat previousAngle = atan2f(previousVec.x, previousVec.y);
                CGFloat angle = atan2f(vec.x, vec.y);
                
                rotation = angle - previousAngle;
            }
            
            
            
            CGAffineTransform transform = self.transform;
            self.transform = CGAffineTransformIdentity;
            
            CGRect frame = self.frame;
            
            
            switch ( _type )
            {
                case Unknown:
                {
                    _activatedAngle += fabsf(rotation);
                    _activatedScale += fabs(previousLen - len);
                    
                    if ( _activatedAngle > kMinActivationAngle )
                    {
                        _type = Rotation;
                    }
                    else if ( _activatedScale > kMinActivationScale )
                    {
                        _type = Scale;
                    }
                    break;
                }
                    
                case Rotation:
                {
                    transform = CGAffineTransformRotate(transform, -rotation);
                    
                    const CGFloat scaleValue = len - previousLen;
                        
                    frame.size.width += scaleValue;
                    frame.size.height += scaleValue;
                    frame.origin.x -= scaleValue * 0.5f;
                    frame.origin.y -= scaleValue * 0.5f;
                    break;
                }
                    
                case Scale:
                {
                    CGPoint scale = CGPointMake(disp.x - previousDisp.x, disp.y - previousDisp.y);
                    
                    if ( location1.x < location2.x &&
                        previousLocation1.x < previousLocation2.x )
                    {
                        scale.x = -scale.x;
                    }
                    
                    if ( location1.y < location2.y &&
                        previousLocation1.y < previousLocation2.y )
                    {
                        scale.y = -scale.y;
                    }
                    
                    frame.size.width += scale.x;
                    frame.size.height += scale.y;
                    frame.origin.x -= scale.x * 0.5f;
                    frame.origin.y -= scale.y * 0.5f;
            
                    break;
                }
            }
            
            
            frame = [self _boundFrame:frame];
            
            self.frame = frame;
            self.transform = transform;
            break;
        }
    }
}
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
//    //duc.tt Reset zoom direction
//    self.previousZoomFactor = CGPointMake(1, 1);
    
    _type = Unknown;
    _activatedAngle = 0.0f;
    _activatedScale = 0.0f;
    [_touches removeObjectsInArray:touches.allObjects];
}
- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
//    //duc.tt Reset zoom direction
//    self.previousZoomFactor = CGPointMake(1, 1);
    
    _type = Unknown;
    _activatedAngle = 0.0f;
    _activatedScale = 0.0f;
    [_touches removeObjectsInArray:touches.allObjects];
}


#pragma mark - private

//duc.tt
- (UIImage*) imageWithImage:(UIImage*) source adjustNewColor:(UIColor *) newColor;
{
    // Create a Core Image version of the image.
    CIImage *sourceImage = [CIImage imageWithCGImage:[source CGImage]];
    
    if (newColor != nil) {
        // Apply a Color Controls - remove color from the image
        CIFilter *removeColorAdjust = [CIFilter filterWithName:@"CIColorControls"];
        [removeColorAdjust setDefaults];
        [removeColorAdjust setValue:[NSNumber numberWithFloat:0.0] forKey:@"inputSaturation"];
        [removeColorAdjust setValue: sourceImage forKey: @"inputImage"];
        CIImage *imageRemovedColor = [removeColorAdjust valueForKey:@"outputImage"];
        
        // Apply a CIHueAdjust filter
        CIFilter *whitePointAdjust = [CIFilter filterWithName:@"CIWhitePointAdjust"];
        [whitePointAdjust setDefaults];
        [whitePointAdjust setValue: imageRemovedColor forKey: @"inputImage"];
        [whitePointAdjust setValue: [CIColor colorWithCGColor:[newColor CGColor]] forKey: @"inputColor"];
        sourceImage = [whitePointAdjust valueForKey: @"outputImage"];
        
        // Set new Colored Image
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef resultRef = [context createCGImage:sourceImage fromRect:[sourceImage extent]];
        [self setNewColoredImage:[UIImage imageWithCGImage:resultRef]]; //duc.tt
        CGImageRelease(resultRef);
    }
    
    // Apply Color Controls for current saturation, brightness, contrast
    float calculatedBrightness = [ self calculatedBrightness];
    CIFilter *colorAdjust = [CIFilter filterWithName:@"CIColorControls"];
    [colorAdjust setDefaults];
    [colorAdjust setValue:[NSNumber numberWithFloat:[self calculatedSaturation]] forKey:@"inputSaturation"];
    [colorAdjust setValue:[NSNumber numberWithFloat:calculatedBrightness] forKey:@"inputBrightness"];
    [colorAdjust setValue:[NSNumber numberWithFloat:[self calculatedContrast]] forKey:@"inputContrast"];
    [colorAdjust setValue: sourceImage forKey: @"inputImage"];
    CIImage *newImage = [colorAdjust valueForKey:@"outputImage"];
    
    // Apply Color Controls for current brightness (exposure)
    float exposure = calculatedBrightness > 0 ? 10*calculatedBrightness : 20*calculatedBrightness;
    colorAdjust = [CIFilter filterWithName:@"CIExposureAdjust"];
    [colorAdjust setDefaults];
    [colorAdjust setValue:[NSNumber numberWithFloat:exposure] forKey:@"inputEV"];
    [colorAdjust setValue: newImage forKey: @"inputImage"];
    newImage = [colorAdjust valueForKey:@"outputImage"];
    
    // Convert the filter output back into a UIImage.
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef resultRef = [context createCGImage:newImage fromRect:[newImage extent]];
    UIImage *result = [UIImage imageWithCGImage:resultRef];
    CGImageRelease(resultRef);
    
    return result;
}

- (CGRect) _activeFrame
{
    CGRect frame = self.handlesView.frame;
    
    if ( _touches.count )
    {
        CGSize bias = CGSizeMake(frame.size.width * 2.0f,
                                 frame.size.height * 2.0f);
        
        frame.origin.x -= bias.width * 0.5f;
        frame.origin.y -= bias.height * 0.5f;
        frame.size.width += bias.width;
        frame.size.height += bias.height;
    }
    
    return frame;
}

- (CGRect) _boundFrame:(CGRect)frame
{
    if ( frame.size.width < _minSize.width )
    {
        CGFloat bias = _minSize.width - frame.size.width;
        frame.size.width += bias;
        frame.origin.x -= bias * 0.5f;
    }
    else if ( frame.size.width > _maxSize.width )
    {
        CGFloat bias = frame.size.width - _maxSize.width;
        frame.size.width -= bias;
        frame.origin.x += bias * 0.5f;
    }
    
    if ( frame.size.height < _minSize.height )
    {
        CGFloat bias = _minSize.height - frame.size.height;
        frame.size.height += bias;
        frame.origin.y -= bias * 0.5f;
    }
    else if ( frame.size.height > _maxSize.height )
    {
        CGFloat bias = frame.size.height - _maxSize.height;
        frame.size.height -= bias;
        frame.origin.y += bias * 0.5f;
    }
    
    
    const CGFloat maxWidth = frame.size.height * 3.0f;
    const CGFloat maxHeight = frame.size.width * 3.0f;
    
    if ( frame.size.width > maxWidth )
    {
        CGFloat bias = frame.size.width - maxWidth;
        frame.size.width -= bias;
        frame.origin.x += bias * 0.5f;
    }
    else if ( frame.size.height > maxHeight )
    {
        CGFloat bias = frame.size.height - maxHeight;
        frame.size.height -= bias;
        frame.origin.y += bias * 0.5f;
    }
    
    return frame;
}

//duc.tt
- (CGFloat)distanceBetweenPoint:(CGPoint) a andPoint:(CGPoint) b
{
    return sqrtf((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y-b.y));
}

@end
