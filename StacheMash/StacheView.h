//
//  StacheView.h
//  StacheMash
//
//  Created by Konstantin Sokolinskyi on 1/19/12.
//  Copyright (c) 2012 Bright Newt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMStache.h"

@class StacheView;

//duc.tt
typedef enum
{
    StacheViewTouchModeNormal = 0,
    StacheViewTouchModeZoom,
    StacheViewTouchModeRotate
} StacheViewTouchMode;

@protocol StacheViewDelegate <NSObject>

- (void)stacheViewTapped: (StacheView*)stacheView;

@end


@interface StacheView : UIView

@property (nonatomic, assign) id<StacheViewDelegate> delegate;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, assign, readonly) BOOL hasMultipleColors;
@property (nonatomic, strong) DMStache *stache;
@property (nonatomic, strong) UIColor *imageColor;  //duc.tt
@property (nonatomic, assign) CGFloat imageSaturation;  //duc.tt saturation in [0, 1]
@property (nonatomic, assign) CGFloat imageBrightness;  //duc.tt brightness in [0, 1]
@property (nonatomic, assign) CGFloat imageContrast;    //duc.tt contrast in [0, 1]
@property (nonatomic, assign) StacheViewTouchMode touchMode;    //duc.tt
//@property (nonatomic, assign) CGPoint previousZoomFactor;  //CGSizeZero if touch ended //duc.tt

- (id)initWithFrame: (CGRect)frame imagesArray: (NSArray*)imagesArray;

- (void)setImage: (UIImage*)newImage;
- (void)setNewColoredImage:(UIImage*)newImage;  //duc.tt
- (float)calculatedSaturation;  //duc.tt
- (float)calculatedBrightness;  //duc.tt
- (float)calculatedContrast;  //duc.tt
- (int)displaySaturation;   //duc.tt
- (int)displayBrightness;   //duc.tt
- (int)displayContrast;   //duc.tt
- (void)refreshImageSettings:(BOOL)inLoop;   //duc.tt Apply saturation, brightness, contrast within current colored image
- (void)setNewStacheImage: (UIImage*)newImage withFrame: (CGRect)frame;
- (void)setNewStacheImageArray: (NSArray*)imagesArray;

- (void)nextStacheColor;

- (void)setColorWithIndex: (NSUInteger)index;
- (NSUInteger)colorCount;


- (void) scaleUp:(CGFloat)scale;
- (void) scaleUpXY:(CGPoint)scale;    //duc.tt
- (UIImage*)image;

@end
