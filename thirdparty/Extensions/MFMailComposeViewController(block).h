//
//  MFMailComposeViewController.h
//  MustacheBash
//
//  Created by Alexander Gavrilko on 07/02/15.
//  Copyright (c) 2015 Bright Newt. All rights reserved.
//

#import <MessageUI/MessageUI.h>

@interface MFMailComposeViewController(block)

- (void) setMailComposeControllerDidFinishWithResult:(void(^)(MFMailComposeViewController *controller, MFMailComposeResult result, NSError *error))block;

@end
