//
//  VoilaViewController.h
//  StacheMash
//
//  Created by Konstantin Sokolinskyi on 1/21/12.
//  Copyright (c) 2012 Bright Newt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
//#import <Sincerely/Sincerely.h>
#import <ShipLib/ShipLib.h>
#import "FacebookShareViewController.h"
#import "FacebookManager.h" 
#import "BaseViewController.h"

//Sun - Fix warnings

@interface VoilaViewController : BaseViewController
    <MFMailComposeViewControllerDelegate,
    FacebookManagerLoginDelegate,
    FacebookShareViewControllerDelegate, FacebookManagerDialogDelegate,
    UIAlertViewDelegate,
    SYSincerelyControllerDelegate,
    UIDocumentInteractionControllerDelegate>

@property (strong, nonatomic) UIImage *sourceImage;
//Sun
@property (strong, nonatomic) UIImage *oriImage;

@property (nonatomic, assign) BOOL fromFbFriendsView;
@property (strong, nonatomic) NSMutableArray *selectedArray;

//Instagram
- (void)shareIG:(id)sender;

@end
