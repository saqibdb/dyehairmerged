//
//  OutputViewController.swift
//  FaceShapeDetection
//
//  Created by Hassan Izhar on 11/18/17.
//  Copyright © 2017 Hassan Izhar. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import Vision

class OutputViewController: UIViewController {

    var shapeLayerFrame = CGRect.zero
    let shapeLayer = CAShapeLayer()
    @IBOutlet weak var outputLabel: UILabel!
    var originalImage: UIImage!
    
    var image: UIImage!
    var shape = ""
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageView.image = image
        outputLabel.text = shape
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    @IBAction func retake(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        print("Backing")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func removeAds(_ sender: Any) {
        self.purchaseProduct(id: "com.look.findfaceshape.removeads")
    }
    
    //MARK: - Purchase a product
    func purchaseProduct(id: String){
        Util.showHud()
        SwiftyStoreKit.purchaseProduct(id, quantity: 1, atomically: true) { result in
            switch result {
            case .success:
                Util.hideHud()
                if !Util.getPurchasedFlag(){
                    Util.setPurchasedFlag(flag: true)
                }
                UserDefaults.standard.synchronize()
            case .error(let error):
                Util.hideHud()
                let title = "Error"
                var message = ""
                switch error.code {
                case .unknown:
                    message = "Unknown error. Please contact support"
                case .clientInvalid:
                    message = "Not allowed to make the payment"
                case .paymentCancelled:
                    message = "cancelled"
                    return
                case .paymentInvalid:
                    message = "The purchase identifier was invalid"
                case .paymentNotAllowed:
                    message = "The device is not allowed to make the payment"
                case .storeProductNotAvailable:
                    message = "The product is not available in the current storefront"
                case .cloudServicePermissionDenied:
                    message = "Access to cloud service information is not allowed"
                case .cloudServiceNetworkConnectionFailed:
                    message = "Could not connect to the network"
                case .cloudServiceRevoked:
                    message = "User has revoked permission to use this cloud service"
                }
                if message == "cancelled"{
                    return
                }
                let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                })
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func downloadAction(_ sender: Any) {
        
        print("Here it is")
        
        
        
        
        let vc : PictureViewController =  PictureViewController(nibName: nil, bundle: nil)
        vc.sourceImage = self.originalImage
        self.navigationController?.pushViewController(vc, animated: true)
        
        //UIApplication.shared.open(URL(string: "https://www.google.com")!, options: [:], completionHandler: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
