When one barber cuts another´s hair, which one does the talking?
Barber: Have you been here before? I don´t remember your face. Man: I have, but it´s healed up now.
Bald guys never have a bad hair day.
I can give you Gisele Bundchen´s haircut, but I can´t give you her face.
How do you get your barber to cut your hair that way?I insult him.
I went into the barber´s and noticed he had dirty hands. I can´t help it,I said the barber. No one´s been in for a shampoo today.
What did the bald man say when he was given a comb? I´ll never part with it. 
What kind of hair do oceans have? Wavy!
Why did the barber win the race? Because he took a short cut.
He has such a long face the barber charges him twice for shaving.
That single bill you stuff into the shampoo person´s hands isn´t doing her any favors. You should tip her at least $3 -- more if your hair is long.
He´s a graduate of the Uncle Fester Keith Moon School of hair styling.
My husband gave me a permanent wave, and now he´s gone.
She has such beautiful hair, every time we go out I insist she wears it.
WHILE getting my hair cut at a neighborhood shop, I asked the barber when would be the best time to bring in my two-year-old son. Without hesitation, the barber answered, "When he's four."
If you´re looking for a therapist, all I have is a tail comb and an opinion.
HAIRDRESSERS, like bartenders, are expected to lend a sympathetic ear as needed. One Friday morning a regular customer came in, sat down and, as I flung the cape around her neck, asked, "Now where did I leave off last week?"