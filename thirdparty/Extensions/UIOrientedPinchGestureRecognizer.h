//
//  UIOrientedPinchGestureRecognizer.h
//  MustacheBash
//
//  Created by Alexander Gavrilko on 08/02/15.
//  Copyright (c) 2015 Bright Newt. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    OrientationNone             = 0,
    OrientationVertical         = 1,
    OrientationHorizontal       = 2,
    OrientationDiagonal         = 3,
    OrientationInvertedDiagonal = 4
}Orientation;


@interface UIOrientedPinchGestureRecognizer : UIPinchGestureRecognizer {
    Orientation _orientation;
    CGFloat     _angle;
    CGPoint     _lastScale;
    CGFloat     _oldScale;
}
//Ориентация жеста
@property (nonatomic) Orientation orientation;

//Угол жеста, относительно OY. В радианах. От 0 до PI. При angle==PI_2 orientation==OrientationHorizontal
@property (nonatomic) CGFloat     angle;
@property (nonatomic) CGPoint     oScale;
@end

#pragma mark c_func
static inline CGFloat degToRad(CGFloat deg) {return deg*M_PI/180;}
static inline CGFloat radToDeg(CGFloat rad) {return rad*180/M_PI;}
//определение косинуса угла между тремя точками. Определяемый угол в точке A, т.е это угол PointB-PointA-PointC

static inline double getCosAngle(CGPoint pointA,CGPoint pointB, CGPoint pointC){
    CGPoint vectorAB = CGPointMake(pointB.x-pointA.x, pointB.y-pointA.y);
    CGPoint vectorAC = CGPointMake(pointC.x-pointA.x, pointC.y-pointA.y);
    
    double dAB=sqrt(vectorAB.x*vectorAB.x+vectorAB.y*vectorAB.y);
    double dAC=sqrt(vectorAC.x*vectorAC.x+vectorAC.y*vectorAC.y);
    
    double scal = vectorAB.x * vectorAC.x +vectorAB.y * vectorAC.y;
    double angle = scal/(dAB*dAC);
    return angle;
}
//определение угла между тремя точками. Определяемый угол в точке A, т.е это угол PointB-PointA-PointC
static inline double getAngle(CGPoint pointA,CGPoint pointB, CGPoint pointC){
    if ((CGPointEqualToPoint(pointA, pointB))||(CGPointEqualToPoint(pointA, pointC))||(CGPointEqualToPoint(pointB, pointC)))
        return 0; //если задано по сути не 3, а 2 точки то возвращаем ноль
    return  acos(getCosAngle(pointA, pointB, pointC));
}
