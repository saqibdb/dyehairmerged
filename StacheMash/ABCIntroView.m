//
//  IntroView.m
//  DrawPad
//
//  Created by Adam Cooper on 2/4/15.
//  Copyright (c) 2015 Adam Cooper. All rights reserved.
//

#import "ABCIntroView.h"
#import "GUIHelper.h"

@interface ABCIntroView () <UIScrollViewDelegate>
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) UIButton *doneButton;

@property (strong, nonatomic) UIView *viewOne;
@property (strong, nonatomic) UIView *viewTwo;
@property (strong, nonatomic) UIView *viewThree;
@property (strong, nonatomic) UIView *viewFour;


@end

@implementation ABCIntroView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        
        [self addSubview:self.scrollView];
        [self addSubview:self.pageControl];
    
        [self.scrollView addSubview:self.viewOne];
        [self.scrollView addSubview:self.viewTwo];
        [self.scrollView addSubview:self.viewThree];
        [self.scrollView addSubview:self.viewFour];
        
        
        //Done Button
        [self addSubview:self.doneButton];
            

    }
    return self;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = CGRectGetWidth(self.bounds);
    CGFloat pageFraction = self.scrollView.contentOffset.x / pageWidth;
    self.pageControl.currentPage = roundf(pageFraction);
    
}

-(UIView *)viewOne {
    if (!_viewOne) {
        NSString* imageName = nil;
        if ([GUIHelper isIpad]) {
            
            if ([GUIHelper isIPadretina]) {
                imageName = @"screenshot2732x2048_1.jpg";
            } else {
                imageName = @"screenshot2048x1536_1.jpg";
            }
            
        } else {
            if ([GUIHelper isPhone5]) {
                imageName = @"screenshot640x1136_1.jpg";
            } else {
                imageName = @"screenshot640x960_1.jpg";
            }
        }
        
        _viewOne = [[UIImageView alloc]initWithFrame:self.frame];
        ((UIImageView*)_viewOne).image = [UIImage imageNamed:imageName];
    }
    return _viewOne;
}

-(UIView *)viewTwo {
    if (!_viewTwo) {
        NSString* imageName = nil;
        if ([GUIHelper isIpad]) {
            
            if ([GUIHelper isIPadretina]) {
                imageName = @"screenshot2732x2048_2.jpg";
            } else {
                imageName = @"screenshot2048x1536_2.jpg";
            }
            
        } else {
            if ([GUIHelper isPhone5]) {
                imageName = @"screenshot640x1136_2.jpg";
            } else {
                imageName = @"screenshot640x960_2.jpg";
            }
        }
        
        CGFloat originWidth = self.frame.size.width;
        CGFloat originHeight = self.frame.size.height;
        
        _viewTwo = [[UIImageView alloc]initWithFrame:CGRectMake(originWidth, 0, originWidth, originHeight)];
        ((UIImageView*)_viewTwo).image = [UIImage imageNamed:imageName];
    }
    return _viewTwo;
    
}

-(UIView *)viewThree{
    
    if (!_viewThree) {
        NSString* imageName = nil;
        if ([GUIHelper isIpad]) {
            
            if ([GUIHelper isIPadretina]) {
                imageName = @"screenshot2732x2048_3.jpg";
            } else {
                imageName = @"screenshot2048x1536_3.jpg";
            }
            
        } else {
            if ([GUIHelper isPhone5]) {
                imageName = @"screenshot640x1136_3.jpg";
            } else {
                imageName = @"screenshot640x960_3.jpg";
            }
        }
        CGFloat originWidth = self.frame.size.width;
        CGFloat originHeight = self.frame.size.height;
        _viewThree = [[UIImageView alloc]initWithFrame:CGRectMake(originWidth*2, 0, originWidth, originHeight)];
        ((UIImageView*)_viewThree).image = [UIImage imageNamed:imageName];
    }
    return _viewThree;
    
}

-(UIView *)viewFour {
    if (!_viewFour) {
    
        NSString* imageName = nil;
        if ([GUIHelper isIpad]) {
            
            if ([GUIHelper isIPadretina]) {
                imageName = @"screenshot2732x2048_4.jpg";
            } else {
                imageName = @"screenshot2048x1536_4.jpg";
            }
            
        } else {
            if ([GUIHelper isPhone5]) {
                imageName = @"screenshot640x1136_4.jpg";
            } else {
                imageName = @"screenshot640x960_4.jpg";
            }
        }
        
        CGFloat originWidth = self.frame.size.width;
        CGFloat originHeight = self.frame.size.height;
        _viewFour = [[UIImageView alloc]initWithFrame:CGRectMake(originWidth*3, 0, originWidth, originHeight)];
        ((UIImageView*)_viewFour).image = [UIImage imageNamed:imageName];
        
    }
    return _viewFour;
    
}

-(UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.frame];
        [_scrollView setDelegate:self];
        [_scrollView setPagingEnabled:YES];
        [_scrollView setContentSize:CGSizeMake(self.frame.size.width*4, self.scrollView.frame.size.height)];
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    return _scrollView;
}

-(UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height-80, self.frame.size.width, 10)];
        [_pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithRed:0.129 green:0.588 blue:0.953 alpha:1.000]];
        [_pageControl setNumberOfPages:4];
    }
    return _pageControl;
}

-(IBAction)onDoneButtonPressed
{
    [self.delegate onDoneButtonPressed:self];
}

-(UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if ([GUIHelper isIpad]) {
            _doneButton.frame = CGRectMake(self.frame.size.width - 100, 0, 100, 100);
        } else {
            _doneButton.frame = CGRectMake(self.frame.size.width - 60, 0, 60, 60);
        }
        [_doneButton addTarget:self action:@selector(onDoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}

@end