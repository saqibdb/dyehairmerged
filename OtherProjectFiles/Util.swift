//
//  Util.swift
//  FaceShapeDetection
//
//  Created by Hassan Izhar on 11/25/17.
//  Copyright © 2017 Hassan Izhar. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class Util: NSObject{
    
    static var hud: MBProgressHUD!
    
    class func setPurchasedFlag(flag: Bool){
        UserDefaults.standard.set(flag, forKey: "isPurchased")
    }
    
    class func getPurchasedFlag() -> Bool{
        return UserDefaults.standard.bool(forKey: "isPurchased")
    }
    
    class func showHud(){
        hud = MBProgressHUD.showAdded(to: (UIApplication.shared.keyWindow)!, animated: true)
        hud.labelText = "Loading"
        hud.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
    }
    
    class func hideHud(){
        hud.hide(true)
    }
}
