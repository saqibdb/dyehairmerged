//
//  FBFriendsCustomViewController.m
//  YourPerfectHairstyle32
//
//  Created by Qaiser Abbas on 6/27/17.
//  Copyright © 2017 APPreciate. All rights reserved.
//

#import "FBFriendsCustomViewController.h"
#import "FacebookManager.h"
#import "StartPageViewController.h"
#import "FacebookShareViewController.h"
#import "VoilaViewController.h"
#import "CustomFriendsTableViewCell.h"

@interface FBFriendsCustomViewController ()

@end

@implementation FBFriendsCustomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
        if ( [[FacebookManager sharedInstance] isLoggedIn] )
        {
            FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends" parameters:@{@"fields": @"name"} HTTPMethod:@"GET"];
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                // TODO: handle results or error of request.
                NSLog(@"request friend list:%@", result);
                dataSource =[NSMutableArray arrayWithArray:result[@"data"]];
                [cutomTableView reloadData];
                
            }];
        }
        else
        {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            [login
             logInWithReadPermissions: @[@"public_profile",@"user_friends"]
             fromViewController:self
             handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                 if (error)
                 {
                     NSLog(@"Process error");
                     
                 }
                 else if (result.isCancelled)
                 {
                     NSLog(@"Cancelled");
                     
                 }
                 else
                 {
                     FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends" parameters:@{@"fields": @"name"} HTTPMethod:@"GET"];
                     [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         // TODO: handle results or error of request.
                         NSLog(@"request friend list:%@", result);
                         dataSource =[NSMutableArray arrayWithArray:result[@"data"]];
                         [cutomTableView reloadData];
                         
                     }];
                 }
             }];

        }

    dataSource = [NSMutableArray array];
    
    selectedMarks = [NSMutableArray new];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods
- (void)done:(id)sender
{
    NSLog(@"%@", selectedMarks);
}

#pragma mark - UITableView Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* Identifier = @"CustomFriendsTableViewCell";
    
    CustomFriendsTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if( cell == nil )
    {
        NSString* cellName = @"CustomFriendsTableViewCell";
        if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            //                cellName = @"LiveEventsTableViewCell-iPad";
        }
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSMutableDictionary*datadict=dataSource[indexPath.row];
    
    [cell.nameLabel setText:datadict[@"name"]];

    [cell.userImage setImageURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=small", datadict[@"id"]]]];
    
    if ([selectedMarks containsObject:datadict])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }

    
    // Check if the cell is currently selected (marked)
    
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary*datadict=dataSource[indexPath.row];
    
    if (self.multipleSelection)
    {
        if ([selectedMarks containsObject:datadict])// Is selected?
            [selectedMarks removeObject:datadict];
        else
            [selectedMarks addObject:datadict];
    }
    else
    {
        if ([selectedMarks containsObject:datadict])
        {
            [selectedMarks removeObject:datadict];
        }
        else
        {
            [selectedMarks removeAllObjects];
            [selectedMarks addObject:datadict];
        }
    }
    
    [cutomTableView reloadData];
    
}

- (IBAction)backButtonTap:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)doneButtonTap:(UIButton *)sender
{
    if ([self.viewName isEqualToString:@"StartPageViewController"])
    {
        [self.navigationController.viewControllers enumerateObjectsUsingBlock:^( id obj, NSUInteger idx, BOOL* stop ) {
            if( [obj isKindOfClass:[StartPageViewController class]] ) {
                StartPageViewController *home=(StartPageViewController*)obj;
                home.selectedArray=selectedMarks;
                home.fromFbFriendsView=YES;
                [self.navigationController popToViewController:obj animated:YES];
                *stop = YES;
            }
        }];
 
    }
    else if ([self.viewName isEqualToString:@"FacebookShareViewController"])
    {
        [self.navigationController.viewControllers enumerateObjectsUsingBlock:^( id obj, NSUInteger idx, BOOL* stop ) {
            if( [obj isKindOfClass:[FacebookShareViewController class]] ) {
                FacebookShareViewController *home=(FacebookShareViewController*)obj;
                home.selectedArray=selectedMarks;
                home.fromFbFriendsView=YES;
                [self.navigationController popToViewController:obj animated:YES];
                *stop = YES;
            }
        }];

    }
    else
    {
        [self.navigationController.viewControllers enumerateObjectsUsingBlock:^( id obj, NSUInteger idx, BOOL* stop ) {
            if( [obj isKindOfClass:[VoilaViewController class]] ) {
                VoilaViewController *home=(VoilaViewController*)obj;
                home.selectedArray=selectedMarks;
                home.fromFbFriendsView=YES;
                [self.navigationController popToViewController:obj animated:YES];
                *stop = YES;
            }
        }];
    }

}
@end
