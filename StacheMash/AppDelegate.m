//
//  AppDelegate.m
//  StacheMash
//
//  Created by Konstantin Sokolinskyi on 1/17/12.
//  Copyright (c) 2012 Bright Newt. All rights reserved.
//

#import <ALSdk.h>
#import <ALInterstitialAd.h>
#import <VungleSDK/VungleSDK.h>
#import "AppDelegate.h"
#import "StartPageViewController.h"
#import "FacebookManager.h"
#import "iRate.h"
#import "Flurry.h"
#import "DataModel.h"
#import "PictureViewController.h"
#import "NavController.h"
#import "TapjoyConnect.h"
#import "ApplicationRater.h"
#import "ALInterstitialAd(scheduling).h"
#import "ABCIntroView.h"
#import <Chartboost/Chartboost.h>

//duc.tt
#import <CommonCrypto/CommonDigest.h>
#import <AdSupport/AdSupport.h>

#if !defined(DEBUG) && !NAG_SCREENS_ON
    #error "set NAG_SCREENS_ON = 1"
#endif

#define NAG_ON_START_UP @"NAG_ON_START_UP"


void exceptionHandler(NSException *exception);

@interface AppDelegate ()<ABCIntroViewDelegate, ChartboostDelegate>

@property (nonatomic, strong) StartPageViewController *startPageViewController;
@property (nonatomic, strong) NavController *navController;
@property (nonatomic, strong) NSMutableArray *jokesArray;
@property (nonatomic, strong) NSURL *iTunesURL;

- (void)logException: (NSException*)exception;

- (void)loadNotificationsJokes;
- (NSString*)randomJoke;
- (void)scheduleNotificationWithTimeInterval: (NSTimeInterval)timeInterval text: (NSString*)text;
- (void)scheduleNotifications;

@end


@implementation AppDelegate

@synthesize window = __window;
@synthesize startPageViewController = _startPageViewController;
@synthesize navController = _navController;
@synthesize jokesArray = _jokesArray;
@synthesize iTunesURL = _iTunesURL;

- (BOOL)application: (UIApplication*)application didFinishLaunchingWithOptions: (NSDictionary*)launchOptions
{
    debug(@"DID finish LOADING");
    NSSetUncaughtExceptionHandler(exceptionHandler);
    
    // Begin a user session. Must not be dependent on user actions or any prior network requests.
    //[Chartboost startWithAppId:@"4f21c409cd1cb2fb7000001b" appSignature:@"92e2de2fd7070327bdeb54c15a5295309c6fcd2d" delegate:self];
    
      [Chartboost startWithAppId:@"59a72d8343150f41674dbd65" appSignature:@"d925a7892b7ed715e9a289f4e5731e386e183f56" delegate:self];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor blackColor];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MainFirstController"];

    /*
    self.startPageViewController =
    [[StartPageViewController alloc] initWithNibName: nil bundle: nil];
    */
    self.navController =
    [[NavController alloc] initWithRootViewController: viewController];
    [self.navController setNavigationBarHidden:YES];
    self.window.rootViewController = self.navController;
    
    [self.window makeKeyAndVisible];
    
    //NSLog(@"Tapdaq send request");
    // Aqui
    //[[Tapdaq sharedSession] setApplicationId:@"542e7b7bbe1c4d465acc9b2e"//@"53593b25fde00bf771000020" //
    //                            clientKey:@"b522663d-5816-49e6-9c9a-2a603f0c52a9"];//@"513cbb9f-9f3f-468a-8fd5-4ba67f5ef811"]; //
    // start Flurry
#if MB_LUXURY
    [Flurry startSession: @"98PS4G4822MDYPGJ8FKQ"];
#else
    [Flurry startSession: FLURRY_SESSION_ID];//@"9U7F7RLY1NDWQEQMUX35";
#endif
    
    [Flurry logAllPageViewsForTarget: self.navController];

#if NAG_SCREENS_ON
    
    [self vungleStart];
    
#endif
    
//    // configure iRate
//    iRate *rate = [iRate sharedInstance];
//
//#if MB_LUXURY
//    rate.appStoreID = 594894839;
//#else
//    rate.appStoreID = APPSTORE_RATE_ID;//499793669;
//#endif
//    
//    rate.debug = NO;
//    rate.remindButtonLabel = nil;
//    rate.message = NSLocalizedString(@"Are you happy with this app?", @"iRate alert text");
//    rate.rateButtonLabel = NSLocalizedString(@"Yes!", @"iRate alert - YES");
//    rate.cancelButtonLabel = NSLocalizedString(@"No :(", @"iRate alert - NO");
//    rate.applicationName = NSLocalizedString(@"Mustache Bash", @"App name in iRate Alert");
//    
//    rate.daysUntilPrompt = 1.0f/24.0f/60.0f/2.0f; // 30 secs
//    
//    rate.usesUntilPrompt = 3;
//    rate.eventsUntilPrompt = 3;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"yes" forKey:@"first_time"];
    [userDefaults synchronize];

    [ApplicationRater awakeWithAgreementHandler:^{
        
        [[DataModel sharedInstance] giftFirstPackIfAvailable];
        
    }];    
    
    [DataModel sharedInstance];
    [FacebookManager sharedInstance];
    
    
#if NAG_SCREENS_ON
    
    debug(@"showing NAG_ON_START_UP");
    if ([DataModel sharedInstance].shouldShowInterstitial){
        [Revmob initWithAppId:[DataModel sharedInstance].revMobFullscreenAppId];
        [Revmob getBanner];

        [Revmob cacheBanner];
        [Revmob setDelegate:self];

        [Revmob cacheNativeStandard:self.window.frame.size.width andHeight:50];
        [Revmob getNativeStandard];
        
        // applovin
        [ALSdk initializeSdk];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //[ALInterstitialAd scheduleAdvertisementForTime:DBL_MAX];
        });
    }
    
#endif
    
#if MB_LUXURY
    
    // TapjoyConnect
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjcConnectSuccess:) name:TJC_CONNECT_SUCCESS object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjcConnectFail:) name:TJC_CONNECT_FAILED object:nil];
	
	[TapjoyConnect requestTapjoyConnect:@"b49e8e4d-4106-4c57-aae8-0737ee74b4aa" secretKey:@"SmgFGkMrrdpGfavbD8VZ"];
    
#endif
    
    
#if NAG_SCREENS_ON
    
    
#endif
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    debug(@"APP will resign active");
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    debug(@"APP did ENTER background");
    
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    debug(@"APP WILL enter FOREGROUND");

    
    if ( _resumeHandler )
    {
        _resumeHandler(self);
        _resumeHandler = nil;
    }
    
    
#if NAG_SCREENS_ON

    debug(@"showing NAG_ON_START_UP");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([DataModel sharedInstance].shouldShowInterstitial){
            [Chartboost showInterstitial:CBLocationHomeScreen];
        }
    });

    
#endif
    
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    debug(@"APP did become ACTIVE");
    
//    [[FacebookManager sharedInstance].facebook extendAccessTokenIfNeeded];
    [self scheduleNotifications];
    
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

#pragma mark - Interstitials support

-(void)vungleStart
{
//    VGUserData*  data  = [VGUserData defaultUserData];
//    NSString*    appID = APP_ID;//@"499793669";
//    
//    // set up config data
//    data.adOrientation   = VGAdOrientationPortrait;
//    
//    // start vungle publisher library
//    [VGVunglePub startWithPubAppID:appID userData:data];
//    [VGVunglePub logToStdout: NO];
    
    NSString* appID = APP_ID;
    VungleSDK* sdk = [VungleSDK sharedSDK];
    
    // start vungle publisher library
    [sdk startWithAppId:appID];
}



// Called when an interstitial has been received, before it is presented on screen
// Return NO if showing an interstitial is currently inappropriate, for example if the user has entered the main game mode
- (BOOL)shouldDisplayInterstitial:(NSString *)location
{
    debug(@"should display interstitial: %@", location);
    debug(@"self.navController.topViewController: %@", self.navController.topViewController);
    
    if ( [location caseInsensitiveCompare: NAG_ON_START_UP]) {
        [self.startPageViewController hideSplash];
        
        if ( [self.navController.topViewController isKindOfClass: [StartPageViewController class]]
            && self.startPageViewController.isCameraShown ) {
            return NO;
        }
        else if ( [self.navController.topViewController isKindOfClass: [PictureViewController class]] ) {
            return NO;
        }
        else {
            return YES;
        }
    }
    else {
        return YES;
    }
}


#pragma mark - Local Notifications

- (void)loadNotificationsJokes
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource: @"Jokes.txt"  ofType: nil];
    NSError *error = nil;
    NSString *jokesString = [NSString stringWithContentsOfFile: filePath encoding: NSUTF8StringEncoding error: &error];
    
    if ( nil != error ) {
        error(@"error reading file '%@': %@", filePath, error);
    }
    
    self.jokesArray = [NSMutableArray arrayWithArray: [jokesString componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]]];
}


- (NSString*)randomJoke
{
    int randomIndex = arc4random() % [self.jokesArray count];
    NSString *joke = [self.jokesArray objectAtIndex: randomIndex];
    [self.jokesArray removeObjectAtIndex: randomIndex];
    
    return joke;
}


- (void)scheduleNotificationWithTimeInterval: (NSTimeInterval)timeInterval text: (NSString*)text
{
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    
    if ( nil == text ) {
        text = @"We've missed you!";
    }
    
    notif.fireDate = [NSDate dateWithTimeIntervalSinceNow: timeInterval];
    notif.timeZone = [NSTimeZone defaultTimeZone];
    notif.alertBody = text;
    notif.alertAction = @"PLAY";
    notif.soundName = UILocalNotificationDefaultSoundName;
    notif.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification: notif];
}


- (void)scheduleNotifications
{
	//LOCAL NOTIFICATIONS
    
    //Cancel all previous Local Notifications
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self loadNotificationsJokes];
    
    //Set new Local Notifications
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        
        debug(@"scheduling local notification");
        
        CGFloat oneDay = 60.0f*60.0f*24.0f;
        
        [self scheduleNotificationWithTimeInterval: oneDay * 3 text: [self randomJoke]];
        [self scheduleNotificationWithTimeInterval: oneDay * 7  text: [self randomJoke]];
        [self scheduleNotificationWithTimeInterval: oneDay * 15  text: [self randomJoke]];
        [self scheduleNotificationWithTimeInterval: oneDay * 30  text: [self randomJoke]];
        [self scheduleNotificationWithTimeInterval: oneDay * 60  text: [self randomJoke]];
    }
}


#pragma mark - External URLS opening

// Pre 4.2 support
//- (BOOL)application: (UIApplication*)application handleOpenURL: (NSURL*)url {
//    
//  //  return [[FacebookManager sharedInstance].facebook handleOpenURL: url];
//    return [FBSession.activeSession handleOpenURL:url];
//}


// For 4.2+ support
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
} 


// Process a URL to something iPhone can handle
- (void)openReferralURL:(NSURL *)referralURL
{
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:referralURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    [conn start];
}

// Save the most recent URL in case multiple redirects occur
// "iTunesURL" is an NSURL property in your class declaration
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
    if (response) {
        NSMutableURLRequest *r = [request mutableCopy]; // original request
        [r setURL: [request URL]];
        
        self.iTunesURL = [r URL];
        if ([self.iTunesURL.host hasSuffix:@"itunes.apple.com"]) {
            [[UIApplication sharedApplication] openURL:self.iTunesURL];
        }
        
        return r;
    }
    else {
        return request;
    }
    
}

#pragma mark - RevMobAdsDelegate


- (void)revmobAdDidReceive
{
    debug(@"did receive RevMob Ad");
}


- (void)revmobAdDidFailWithError:(NSError *)error
{
    error(@"did fail to receive RevMob Ad with error: %@", error);
}


- (void)revmobUserClickedInTheCloseButton
{
    debug(@"user closed RevMob AD");
}


- (void)revmobUserClickedInTheAd
{
    debug(@"user clicked in RevMob AD");
}


#pragma mark TapjoyConnect Observer methods

-(void) tjcConnectSuccess:(NSNotification*)notifyObj
{
	debug(@"Tapjoy Connect Succeeded");
}

-(void) tjcConnectFail:(NSNotification*)notifyObj
{
	debug(@"Tapjoy Connect Failed");
}

//Tapdaq
-(void)willDisplayInterstitial{
    NSLog(@"Will display Interstitial");
}

- (void)didFailToLoadInterstitial{
    NSLog(@"did Fail To Load Interstitial");
}

- (void)didDisplayInterstitial{
     NSLog(@"did display Interstitial");
}
#pragma mark - Exception Handling

void exceptionHandler(NSException *exception)
{
    [Flurry logError: @"Uncaught exception" message: @"Crash!" exception: exception];
    
#if DEBUG    
	[(AppDelegate*)[UIApplication sharedApplication].delegate logException: exception];
#endif
}


- (void)logException:(NSException*)exception
{
	NSArray *addresses = [exception callStackReturnAddresses];
	NSString *logging=[NSString stringWithFormat:@"********************\nException"];
	UIDevice *d=[UIDevice currentDevice];
	NSDictionary *info=[[NSBundle mainBundle]infoDictionary];
	NSString *locale = [[NSLocale currentLocale] localeIdentifier];
	NSString *version = [info objectForKey:@"CFBundleVersion"];
	
    logging=[logging stringByAppendingFormat:@"\n*********\nModel:\t\t%@\nLocalized:\t%@\nSystemName:\t%@\nSystemVersion:\t%@\nVersion:\t%@\nLocale:\t\t%@",d.model,d.localizedModel,d.systemName,d.systemVersion,version,locale];
    
	logging=[logging stringByAppendingFormat:@"\n*********\nName:\t\t%@\nReason:\t\t%@\nUserInfo:\n",exception.name,exception.reason];
	
	//Append the dictionary
	for(NSObject *key in [exception.userInfo allKeys])
	{
		logging=[logging stringByAppendingFormat:@"\nuserInfo: %@ => %@\n",key,[exception.userInfo objectForKey:key]];
	}
	
    if (addresses)
	{
		NSString *command = @"*********\nGet stack trace with (leave app running): /usr/bin/atos -p ";
        NSString *pid = [[NSNumber numberWithInt:[[NSProcessInfo processInfo] processIdentifier]] stringValue];
		
		command = [command stringByAppendingString:pid];
		command = [command stringByAppendingString:@" "];
		
		for(NSNumber *number in addresses)
		{
			command = [command stringByAppendingFormat:@" %u", [number intValue]];
		}
		
		logging=[logging stringByAppendingString:command];
		debug(@"%@", logging);
    }
	else
	{
		logging=[logging stringByAppendingString:@"*********\nNo stack trace available"];
        debug(@"%@", logging);
    }	
}


+(void)showIntroView:(UIViewController*)parentView
{
    ABCIntroView* introView = [[ABCIntroView alloc] initWithFrame:parentView.view.frame];
    introView.delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    introView.backgroundColor = [UIColor colorWithWhite:0.149 alpha:1.000];
    [parentView.view addSubview:introView];
}

#pragma mark - ABCIntroViewDelegate Methods

-(void)onDoneButtonPressed:(ABCIntroView*)caller {
    
    //    Uncomment so that the IntroView does not show after the user clicks "DONE"
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES"forKey:@"intro_screen_viewed"];
    [defaults synchronize];
    
    
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        caller.alpha = 0;
    } completion:^(BOOL finished) {
        [caller removeFromSuperview];
    }];
}
@end
