//
//  UIOrientedPinchGestureRecognizer.m
//  MustacheBash
//
//  Created by Alexander Gavrilko on 08/02/15.
//  Copyright (c) 2015 Bright Newt. All rights reserved.
//

#import <UIKit/UIGestureRecognizerSubclass.h>
#import "UIOrientedPinchGestureRecognizer.h"

@implementation UIOrientedPinchGestureRecognizer
@synthesize angle=_angle, orientation=_orientation,oScale=_lastScale;

#pragma mark init/dealloc

-(id) init{
    self = [super init];
    if (self) {
        _orientation = OrientationNone;
        _angle       = 0;
    }
    return self;
}

#pragma mark - Methods
-(void) detectAngle:(NSArray *) touches{
    if ([touches count]!=2) {
        _orientation= OrientationNone;
        _angle = 0;
        //вслучае больше двух пальцев считаем
        //что все коэф. были увеличины
        _lastScale.x *= self.scale/_oldScale;
        _lastScale.y *= self.scale/_oldScale;
        
        _oldScale = self.scale;
        return;
    }
    //Получим две точки
    CGPoint firstPoint =[[touches objectAtIndex:0] locationInView:self.view];
    CGPoint secondPoint = [[touches objectAtIndex:1] locationInView:self.view];
    
    //определим ближайшую к OY
    if (secondPoint.x<firstPoint.x) {
        CGPoint tmp = secondPoint;
        secondPoint = firstPoint;
        firstPoint = tmp;
    }
    
    //Найдем угол
    _angle = getAngle(firstPoint,secondPoint,CGPointMake(firstPoint.x,firstPoint.y-100.0f));
    
    //по углу определим ориентацию
    if ((_angle> M_PI_4/2.0f)&&(_angle< M_PI_4+M_PI_4/2.0f)) {
        _orientation=   OrientationDiagonal;
        _lastScale.x *= self.scale/_oldScale;
        _lastScale.y *= self.scale/_oldScale;
    }else
        if ((_angle> M_PI_4+M_PI_4/2.0f)&&(_angle<M_PI_2+M_PI_4/2.0f)) {
            _orientation = OrientationHorizontal;
            _lastScale.x *= self.scale/_oldScale;
        }else
            if ((_angle>M_PI_2+M_PI_4/2.0f)&&(_angle<M_PI_2+M_PI_4+M_PI_4/2.0f)){
                _orientation = OrientationInvertedDiagonal;
                _lastScale.x *= self.scale/_oldScale;
                _lastScale.y *= self.scale/_oldScale;
            }else{
                _orientation=OrientationVertical;
                _lastScale.y *= self.scale/_oldScale;
            }
    _oldScale=self.scale;
    return;
    
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    _lastScale = CGPointMake(1.0f, 1.0f);
    _oldScale = 1.0f;
    
    [self detectAngle:[touches allObjects]];
    [super touchesBegan:touches withEvent:event];
}

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [self detectAngle:[touches allObjects]];
    [super touchesMoved:touches withEvent:event];
}


@end
