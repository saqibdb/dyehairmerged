//
//  UIAlertView(block).h
//  Capsule
//
//  Created by Gavrilko Alexander on 13.08.14.
//  Copyright (c) 2014 CodeIT. All rights reserved.
//

@interface UIAlertView(block)

/*!
 You can scip button handlers buy setting nil value, but it always should be finished with nil key.
 
 @code
 RIGHT:
 [[UIAlertView alloc] initWithTitle:@"title"
                            message:@"message"
            buttonTitlesAndHandlers:@"Ok", nil, // nil action for "Ok" button
                                    nil]; // nil terminated key
 
 WRONG:
 [[UIAlertView alloc] initWithTitle:@"title"
                            message:@"message"
            buttonTitlesAndHandlers:@"Ok", nil];
 @endcode
 
 */
- (id) initWithTitle:(NSString *)title message:(NSString *)message buttonTitlesAndHandlers:(NSString*)key, ... NS_REQUIRES_NIL_TERMINATION;

@end
