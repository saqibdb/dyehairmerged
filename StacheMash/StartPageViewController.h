//
//  StartPageViewController.h
//  StacheMash
//
//  Created by Konstantin Sokolinskyi on 1/17/12.
//  Copyright (c) 2012 Bright Newt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookManager.h"
#import "BaseViewController.h"
#import "MBProgressHUD.h"
#import "PurchasePopupVC.h"
@interface StartPageViewController : BaseViewController
    <UIImagePickerControllerDelegate,
        UINavigationControllerDelegate,
        UINavigationControllerDelegate,
        FacebookManagerLoginDelegate,
        MBProgressHUDDelegate,
        UISearchBarDelegate,PurchasePopupDelegate>

@property (nonatomic, assign) BOOL shouldShowSplashLoading;
@property (nonatomic, assign) BOOL isCameraShown;
@property (nonatomic, assign) BOOL fromFbFriendsView;
@property (strong, nonatomic) NSMutableArray *selectedArray;

- (void)hideSplash;

@end
