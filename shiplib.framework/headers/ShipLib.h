//
//  ShipLib.h
//  Sincerely
//
//  Created by Sincerely on 7/5/11.
//  Copyright 2013 Sincerely, Inc. All rights reserved.
//
//  Documentation: https://github.com/sincerely/shiplib-ios-framework
//

#define kSHIPLIB_VERSION @"2.0.5"

#import <ShipLib/SYConstants.h>
#import <ShipLib/SYSincerelyController.h>
