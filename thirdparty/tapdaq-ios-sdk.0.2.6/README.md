

# Tapdaq SDK

Thank you for downloading the Tapdaq SDK! The installation is very
simple, here's a quick how-to guide.

If you haven’t already signed up to Tapdaq, please visit: https://www.tapdaq.com/

### 1. Download the SDK

You can download the SDK from https://tapdaq.com/assets/sdk/tapdaq-ios-sdk.0.2.6.zip and
unzip the package.

### 2. Add Dependencies

Open your project in Xcode, click on the project file, and select the
target of the application, then click 'Build Phases'. 

Please add the following frameworks under the 'Link Binary With
Libraries' section:

- Foundation (required)
- Security (required)
- SystemConfiguration (required)
- MobileCoreServices (required)
- QuartzCore (required)
- CoreGraphics (required)
- AdSupport (optional)

### 3. Add Framework

Drag the unzipped `Tapdaq.framework` folder into your project. We
recommend that you tick 'Copy items into destination group's folder'.

### 4. Authenticate with Tapdaq

Once you have added the framework, next you need to add the following
`#import` statement at the top of your `AppDelegate.m` file:

    #import <Tapdaq/Tapdaq.h>

Navigate to the following method in your `AppDelegate.m` file and add
the following command:

    - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    {
        [[Tapdaq sharedSession] setApplicationId:@"YOUR_TAPDAQ_APP_ID"
                                       clientKey:@"YOUR_TAPDAQ_CLIENT_KEY"];
        
        return YES;
    }


Please replace `YOUR_TAPDAQ_APP_ID` and `YOUR_TAPDAQ_CLIENT_KEY` with the
details that you should have received in an email, please check your
inbox. 

Build your application to test it is installed correctly.

### Next Steps

To learn how to display our adverts inside your application, please visit the docs at: https://tapdaq.com/docs

This is an early release of the Tapdaq SDK, once we have 200
applications active we will be launching the web dashboard where you
can take full control of what this SDK offers.

If you have any questions, or wish to learn more, please email
support@tapdaq.com

