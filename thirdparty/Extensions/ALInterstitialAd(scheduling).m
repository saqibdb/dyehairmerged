//
//  ALInterstitialAd(scheduling).m
//  MustacheBash
//
//  Created by Alexander Gavrilko on 21/02/15.
//  Copyright (c) 2015 Bright Newt. All rights reserved.
//

#import "NSTimer(block).h"
#import "ALInterstitialAd(scheduling).h"

@implementation ALInterstitialAd(scheduling)

+ (BOOL) scheduleAdvertisementForTime:(NSTimeInterval)timeInterval
{
    static NSTimeInterval currentInterval;
    static NSTimeInterval timerInterval;
    static NSTimer *scheduledTimer;
    
    BOOL didShow = NO;
    
    
    if ( timeInterval >= 0.00 )
    {
        if ( [self isReadyForDisplay] )
        {
            [self show];
            [scheduledTimer setFireDate:[NSDate distantFuture]];
            
            didShow = YES;
        }
        else
        {
            if ( !scheduledTimer )
            {
                static const NSTimeInterval kRepeatInterval = 0.05;
                
                currentInterval = 0;
                timerInterval = timeInterval;
                scheduledTimer = [NSTimer scheduledTimerWithTimeInterval:kRepeatInterval block:^(NSTimer *sender) {
                    
                    if ( [self isReadyForDisplay] )
                    {
                        [self show];
                        
                        [sender setFireDate:[NSDate distantFuture]];
                    }
                    else
                    {
                        currentInterval += kRepeatInterval;
                        if ( currentInterval > timerInterval )
                        {
                            [sender setFireDate:[NSDate distantFuture]];
                            
#ifdef DEBUG
                            NSLog(@"%@: fail to show advertisement: Time is up (%.10lf).", [self class], currentInterval);
#endif
                        }
                    }
                    
                } userInfo:nil repeats:YES];
            }
            else
            {
                currentInterval = 0;
                timerInterval = timeInterval;
                [scheduledTimer setFireDate:[NSDate date]];
            }
        }
    }
    else
    {
        [scheduledTimer setFireDate:[NSDate distantFuture]];
    }
    
    return didShow;
}


+ (void) unscheduleCurrentAdvertisement
{
    [self scheduleAdvertisementForTime:-1.00];
}

@end
