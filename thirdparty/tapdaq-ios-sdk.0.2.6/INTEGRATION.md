# Tapdaq Integration Guide

**The Tapdaq SDK is the workhorse behind our whole network and economy. Whilst you don't have to install the SDK in order to participate on the platform - if you do install the SDK, you'll have the opportunity to earn a lot more Daq than people/companies who haven't**

Please make sure you have installed the SDK before proceeding with this document, if you haven't done so already, please visit our [5-minute quickstart guide](https://www.tapdaq.com/quickstart)

## Interstitials

Interstitials are full-screen visuals that appear inside your application. Consumers of your app will either tap the creative or tap the close button.

To begin preparations for displaying an interstitial, navigate to the header file that you want the interstitial to appear in (e.g. ```MyViewController.h```) and add the following import statement at the top of the file:

	#import <Tapdaq/Tapdaq.h>

Please note that you only need to provide imort statement once *per header file*. Next, add the following command to the implementation file (e.g. ```MyViewController.m```) inside a method of your choice:

	[[Tapdaq sharedSession] showInterstitial];

Test and run your application, and check to see if it's working. If your Tapdaq account is not subscribed to an active interstitial campaigns, a 'More Apps' popup will appear instead.

## More Apps

The More Apps popup is the same size as an interstitial, except that it serves two different types of visuals; the first is at the top of the popup, which we call a 'featured ad'. Below that we have automatic adverts, which are other app icons and names who are using Tapdaq.

Similar to the interstitials, we need to provide an import statement to the header file (e.g. ```MySecondViewController.h```) you want the More Apps popup to appear in. 

	#import <Tapdaq/Tapdaq.h>

Next add the following command to the implementation file (e.g. ```MySecondViewController.m```) inside a method of your choice:

**[[Tapdaq sharedSession] showMoreApps];**

## Traditional Banners

Traditional banners are fixed banners that take up a small portion of the screen real estate. Consumers of your app can tap to visit the app store page of the app being advertised.

As with the other two adverts, we also need to provide an import statement to the header file (e.g. ```MyThirdViewController.h```):

	#import <Tapdaq/Tapdaq.h>

### Simple integration

The simplest way to add a traditional banner is to add the following statement to a method in the implementation file (e.g. ```MyThirdViewController.m```):

	[[Tapdaq sharedSession] showTraditional:self.view];

Where ```self.view``` is a ```UIView``` object. This command will add a traditional banner as a subview positioned at the top of the ```UIView```.

### Optional integration

If you would like the advert to appear at the bottom of the ```UIView``` then use the following command:

	[[Tapdaq sharedSession] showTraditional:self.view 
    							 atPosition:TQTraditionalPositionBottom];

If you would like to add a traditional banner to the top of the ```UIView``` but offset it's position 15 points downwards, execute the following command:

	[[Tapdaq sharedSession] showTraditional:self.view
								 atPosition:TQTraditionalPositionTop
								 withOffset:15.0f];

Similarly if you would like the traditional banner at the bottom of the ```UIView``` but offset it's position 10 points upwards, execute the following command:

	[[Tapdaq sharedSession] showTraditional:self.view 
								 atPosition:TQTraditionalPositionBottom
								 withOffset:10.0f];

We've designed our SDK with simplicity in mind and we hope you find it easy to integrate.

Any questions at all, please feel free to nudge Nick, our CTO through his email; nick@tapdaq.com