//
//  StartPageViewController.m
//  StacheMash
//
//  Created by Konstantin Sokolinskyi on 1/17/12.
//  Copyright (c) 2012 Bright Newt. All rights reserved.
//


#import <UIKit/UIImagePickerController.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <Tapdaq/Tapdaq.h>
#import "AppDelegate.h"
#import "StartPageViewController.h"
#import "PictureViewController.h"
#import "InfoViewController.h"
#import "GUIHelper.h"
#import "FacebookManager.h"
#import "ASIHTTPRequest.h"
#import "iRate.h"
#import "Flurry.h"
#import "ALInterstitialAd(scheduling).h"
#import "PurchasePopupVC.h"
#import "FBFriendsCustomViewController.h"

static NSTimeInterval kTimerTimeout = 7.0;


@interface StartPageViewController () <WebViewControllerDelegate>
{
    EFacebookAPICall _currentAPICall;
    
}
@property (strong, nonatomic) UIButton *infoButton;
@property (strong, nonatomic) UIButton *coolStuffButton;
@property (strong, nonatomic) UIButton *takePictureButton;
@property (strong, nonatomic) UIButton *openLibraryButton;

@property (strong, nonatomic) UIImageView *splashScreen;
@property (strong, nonatomic) NSTimer *timeoutTimer;

@property (retain, nonatomic) FBFriendsCustomViewController *friendPickerController;
@property (retain, nonatomic) UISearchBar *searchBar;
@property (retain, nonatomic) NSString *searchText;
@property (assign, nonatomic) BOOL showingFriendPicker;

//Quang ipad
@property (retain, nonatomic) UIPopoverController *popover;

//End

// FB friend profile picture hud
@property (nonatomic, strong) MBProgressHUD *hud;


@property (nonatomic, strong) UIImageView *animationImageView;
@property (nonatomic, strong) CADisplayLink *displayLink;
@property (nonatomic) NSInteger frameNumber;
@property (nonatomic, strong) NSArray *imagesArray;


- (void)closeModalViews: (NSNotification*)info;

- (void)goInfo: (id)sender;
- (void)goCoolStuff: (id)sender;
- (void)goToStache: (id)sender;

- (void)pickImageFromLibrary: (id)sender;
- (void)takePicture: (id)sender;
- (void)openImagePickerWithSourceType: (UIImagePickerControllerSourceType)sourceType;

@end



@implementation StartPageViewController

@synthesize infoButton = _infoButton;
@synthesize coolStuffButton = _coolStuffButton;
@synthesize takePictureButton = _takePictureButton;
@synthesize openLibraryButton = _openLibraryButton;
@synthesize splashScreen = _splashScreen;
@synthesize shouldShowSplashLoading = __shouldShowSplashLoading;
@synthesize timeoutTimer = _timeoutTimer;
@synthesize isCameraShown = __isCameraShown;

@synthesize friendPickerController = _friendPickerController;
@synthesize searchBar = _searchBar;
@synthesize searchText = _searchText;
@synthesize showingFriendPicker = _showingFriendPicker;

@synthesize hud = _hud;
//Quang ipad

@synthesize popover = _popover;

//end



#pragma mark - Initialization


- (id)initWithNibName: (NSString*)nibNameOrNil bundle: (NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil];
    if ( self ) {
        self.wantsFullScreenLayout = YES;
        self.shouldShowSplashLoading = YES;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

- (void)loadView
{
	self.view = [[UIView alloc] initWithFrame: [UIScreen mainScreen].applicationFrame];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    self.isCameraShown = NO;
        _fromFbFriendsView=NO;
    // BG image
    UIImageView *bgImageVIew = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    if ([GUIHelper isPhone4]) {
        [bgImageVIew setFrame:CGRectMake(0, -30, self.view.frame.size.width, self.view.frame.size.height)];
    }
    
#if MB_LUXURY
    //iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        bgImageVIew.image = [UIImage imageNamed: @"MBLuxury-home-568h.png"];
    }
    else if ( [GUIHelper isPhone5] ) {
        bgImageVIew.image = [UIImage imageNamed: @"MBLuxury-home-568h.png"];
    }
    else {
        bgImageVIew.image = [UIImage imageNamed: @"MBLuxury-home.png"];
    }
#else
    //iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        if ([GUIHelper isIPadretina])
        bgImageVIew.image = [UIImage imageNamed: @"splash-bg-ipad@2x~ipad.png"];
        else
            bgImageVIew.image = [UIImage imageNamed: @"splash-bg-ipad.png"];
    }
    else if ( [GUIHelper isPhone5] ) {
        bgImageVIew.image = [UIImage imageNamed: @"splash-bg-568h.png"];
    }
    else {
        bgImageVIew.image = [UIImage imageNamed: @"splash-bg.png"];
    }
#endif
    
    [self.view addSubview: bgImageVIew];
    
    // INFO button
    UIImage *infoButtonImage, *infoButtonImagePressed;
    // iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        infoButtonImage		= [UIImage imageNamed: @"btn-i-ipad.png"];
        infoButtonImagePressed	= [UIImage imageNamed: @"btn-i-ipad-press.png"];
    }else{
        infoButtonImage		= [UIImage imageNamed: @"btn-i.png"];
	    infoButtonImagePressed	= [UIImage imageNamed: @"btn-i-press.png"];
    }
	
	self.infoButton = [UIButton buttonWithType: UIButtonTypeCustom];
	[self.infoButton setBackgroundImage: infoButtonImage forState: UIControlStateNormal];
	[self.infoButton setBackgroundImage: infoButtonImagePressed forState: UIControlStateHighlighted];
    //iPad support
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.infoButton.frame= CGRectMake(122, 90,  infoButtonImage.size.width,   infoButtonImage.size.height);
    }
    else
    {
        if ( [GUIHelper isPhone5] ) {
            self.infoButton.frame= CGRectMake(35, 45, infoButtonImage.size.width, infoButtonImage.size.height);

        }else{
            self.infoButton.frame= CGRectMake(35, 15, infoButtonImage.size.width, infoButtonImage.size.height);        }

    }
    
	//self.infoButton.frame= CGRectMake(35, 45, infoButtonImage.size.width, infoButtonImage.size.height);
	
	[self.infoButton addTarget: self action: @selector(goInfo:) forControlEvents: UIControlEventTouchUpInside];
    [self.view addSubview: self.infoButton];
    
    // COOL STUFF button
    UIImage *coolButtonImage, *coolButtonImagePressed;
    // iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        coolButtonImage		= [UIImage imageNamed: @"btn-more-ipad.png"];
        coolButtonImagePressed	= [UIImage imageNamed: @"btn-more-ipad-press.png"];
    }else{
        coolButtonImage		= [UIImage imageNamed: @"btn-more.png"];
	    coolButtonImagePressed = [UIImage imageNamed: @"btn-more-press.png"];
    }
    self.coolStuffButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [self.coolStuffButton setBackgroundImage: coolButtonImage forState: UIControlStateNormal];
	[self.coolStuffButton setBackgroundImage: coolButtonImagePressed forState: UIControlStateHighlighted];
    
    //iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.coolStuffButton.frame= CGRectMake(510, 90, coolButtonImage.size.width, coolButtonImage.size.height);
    }
    else
    {
        if ( [GUIHelper isPhone5] ) {
            self.coolStuffButton.frame = CGRectMake(225, 45, coolButtonImage.size.width,  coolButtonImage.size.height);
            
        }else{
            self.coolStuffButton.frame = CGRectMake(225, 15, coolButtonImage.size.width,  coolButtonImage.size.height);
        }

    }
    
    [self.coolStuffButton addTarget: self
                             action: @selector(goCoolStuff:)
                   forControlEvents: UIControlEventTouchUpInside];
    
    [self.view addSubview: self.coolStuffButton];
    
    
    
    [self.view addSubview:self.animationImageView];

//    NSArray *animationArray=[NSArray arrayWithObjects:
//                             [UIImage imageNamed:@"image_1.png"],
//                             [UIImage imageNamed:@"image_2.png"],
//                             [UIImage imageNamed:@"image_3.png"],
//                             [UIImage imageNamed:@"image_4.png"],
//                             nil];
//    UIImageView *animationView=[[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-180)/2, [GUIHelper getBottomYForView:self.coolStuffButton]+20,180, 180)];
//    animationView.backgroundColor=[UIColor purpleColor];
//    animationView.animationImages=animationArray;
//    animationView.animationDuration=2.5;
//    animationView.animationRepeatCount=0;
//    [animationView startAnimating];
//    [self.view addSubview:animationView];

    
    
    CGFloat buttonsOriginY = 300;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        buttonsOriginY += 300;
    }
    else
    {
      if ( [GUIHelper isPhone5] ) {
        buttonsOriginY += 40;
      }else{
          buttonsOriginY = 290;
      }
    }

    // TAKE PICTURE button
    UIImage *takePictureButtonImage, *takePictureButtonImagePressed;
    // iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        takePictureButtonImage		= [UIImage imageNamed: @"btn-take-picture-ipad.png"];
        takePictureButtonImagePressed	= [UIImage imageNamed: @"btn-take-picture-ipad-press.png"];
    }else{

         takePictureButtonImage         = [UIImage imageNamed: @"btn-take-picture.png"];
	     takePictureButtonImagePressed  = [UIImage imageNamed: @"btn-take-picture-press.png"];
    }
    self.takePictureButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [self.takePictureButton setBackgroundImage: takePictureButtonImage forState: UIControlStateNormal];
	[self.takePictureButton setBackgroundImage: takePictureButtonImagePressed forState: UIControlStateHighlighted];
    
    //iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.takePictureButton.frame = CGRectMake(378, buttonsOriginY,
                                                   takePictureButtonImage.size.width,
                                                   takePictureButtonImage.size.height);
    }
    else
    {
        self.takePictureButton.frame = CGRectMake(160, buttonsOriginY,
                                                  takePictureButtonImage.size.width,
                                                  takePictureButtonImage.size.height);
    }
    
   
    [self.takePictureButton addTarget: self
                               action: @selector(takePicture:)
                     forControlEvents: UIControlEventTouchUpInside];
    
    [self.view addSubview: self.takePictureButton];

    // OPEN LIBRARY button
    UIImage *openLibraryButtonImage, *openLibraryButtonImagePressed;
    // iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        openLibraryButtonImage		= [UIImage imageNamed: @"btn-open-picture-ipad.png"];
        openLibraryButtonImagePressed	= [UIImage imageNamed: @"btn-open-picture-ipad-press.png"];
    }else{

        openLibraryButtonImage		= [UIImage imageNamed: @"btn-open-picture.png"];
	    openLibraryButtonImagePressed = [UIImage imageNamed: @"btn-open-picture-press.png"];
    }
    self.openLibraryButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [self.openLibraryButton setBackgroundImage: openLibraryButtonImage forState: UIControlStateNormal];
	[self.openLibraryButton setBackgroundImage: openLibraryButtonImagePressed forState: UIControlStateHighlighted];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.openLibraryButton.frame = CGRectMake(40, buttonsOriginY,
                                                   openLibraryButtonImage.size.width,
                                                   openLibraryButtonImage.size.height);
    }
    else
    {
        self.openLibraryButton.frame = CGRectMake(3, buttonsOriginY,
                                                  openLibraryButtonImage.size.width,
                                                  openLibraryButtonImage.size.height);
    }
//    self.openLibraryButton.frame = CGRectMake(3, buttonsOriginY,
//                                                                                                openLibraryButtonImage.size.width,
//                                                                                              openLibraryButtonImage.size.height);
    
    [self.openLibraryButton addTarget: self
                               action: @selector(pickImageFromLibrary:)
                     forControlEvents: UIControlEventTouchUpInside];
    
    [self.view addSubview: self.openLibraryButton];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(closeModalViews:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    
    // FB PICTURE button
    UIImage *fbPictureButtonImage, *fbPictureButtonImagePressed;
    // iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        fbPictureButtonImage		= [UIImage imageNamed: @"btn-pickfriend-ipad.png"];
        fbPictureButtonImagePressed	= [UIImage imageNamed: @"btn-pickfriend-ipad-press.png"];
    }else{
         fbPictureButtonImage         = [UIImage imageNamed: @"btn-pickfriend.png"];
	     fbPictureButtonImagePressed  = [UIImage imageNamed: @"btn-pickfriend-press.png"];
    }
    UIButton *fbPictureButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [fbPictureButton setBackgroundImage: fbPictureButtonImage forState: UIControlStateNormal];
	[fbPictureButton setBackgroundImage: fbPictureButtonImagePressed forState: UIControlStateHighlighted];
    
    fbPictureButton.frame = CGRectMake(0.5 * (self.view.frame.size.width - fbPictureButtonImage.size.width), [GUIHelper getBottomYForView:self.takePictureButton] + ([GUIHelper isPhone5]? 10 : 5),
                                                                                   fbPictureButtonImage.size.width,
                                                                                  fbPictureButtonImage.size.height);
    [fbPictureButton addTarget: self
                        action: @selector(getFBPicture:)
              forControlEvents: UIControlEventTouchUpInside];
    
//    banner640x
    
    [self.view addSubview: fbPictureButton];

    
    
    UIImage *bannerView;
    // iPad support
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        if ([GUIHelper isIPadretina])
            bannerView = [UIImage imageNamed: @"banner2048x.jpg"];
        else
            bannerView = [UIImage imageNamed: @"banner2048x.jpg"];
    }
    else if ( [GUIHelper isPhone5] ) {
        bannerView = [UIImage imageNamed: @"banner640x.jpg"];
    }
    else {
        bannerView = [UIImage imageNamed: @"banner640x.jpg"];
    }

    UIButton *BannerViewButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [BannerViewButton setBackgroundImage: bannerView forState: UIControlStateNormal];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        BannerViewButton.frame = CGRectMake(0, [GUIHelper getBottomYForView:fbPictureButton]+5,
                                            bannerView.size.width,
                                            150);
    }
    else
    {
        if ([GUIHelper isPhone5])
        {
            BannerViewButton.frame = CGRectMake(0, [GUIHelper getBottomYForView:fbPictureButton] + ([GUIHelper isPhone5]? 10 : 5),
                                                bannerView.size.width,
                                                bannerView.size.height);
        }
        else
        {
            BannerViewButton.frame = CGRectMake(0, [GUIHelper getBottomYForView:fbPictureButton] + ([GUIHelper isPhone5]? 10 : 5),
                                                bannerView.size.width,
                                                60);
        }
    }

    [BannerViewButton addTarget: self
                        action: @selector(bannerTapped:)
              forControlEvents: UIControlEventTouchUpInside];
    
    //    banner640x
    
//    [self.view addSubview: BannerViewButton];

//    // HAPPY BIRTHDAY button
//    UIButton *happyBirthdayButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
//    [happyBirthdayButton setTitle: @"Happy Birthday" forState: UIControlStateNormal];
//    happyBirthdayButton.frame = CGRectMake(0, 0, 200, 50);
//    happyBirthdayButton.center = CGPointMake(160, 430);
//    [happyBirthdayButton addTarget: self action: @selector(happyBirhthday:) forControlEvents: UIControlEventTouchUpInside];
//    
//    [self.view addSubview: happyBirthdayButton];
    
    
    //NSLog(@"Tapdaq send request");
    //[[Tapdaq sharedSession] setApplicationId:@"542e7b7bbe1c4d465acc9b2e"//@"53593b25fde00bf771000020" //
    //clientKey:@"b522663d-5816-49e6-9c9a-2a603f0c52a9"];//@"513cbb9f-9f3f-468a-8fd5-4ba67f5ef811"];
   
    
    // Start screen postponement
#if NAG_SCREENS_ON
    
    if ( self.shouldShowSplashLoading ) {
        debug(@"adding SPLASH loading");
      
        UIImage *splashImage;
        CGFloat spinnerFactor;
#if MB_LUXURY
        // iPad support
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            splashImage = [UIImage imageNamed: @"MBLuxury-default-Portrait~ipad.png"];
        }
        else if ( [GUIHelper isPhone5] ) {
            splashImage = [UIImage imageNamed: @"MBLuxury-default-568h@2x.png"];
        }
        else {
            splashImage = [UIImage imageNamed: @"MBLuxury-default.png"];
        }
        
        spinnerFactor = 0.85;
#else
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            splashImage = [UIImage imageNamed: @"Default-Portrait~ipad.png"];
            spinnerFactor = 0.9;
        }

        else if ( [GUIHelper isPhone5] ) {
            splashImage = [UIImage imageNamed: @"Default-568h@2x.png"];
            spinnerFactor = 0.85;
        }
        else {
            splashImage = [UIImage imageNamed: @"Default.png"];
            spinnerFactor = 0.9;
        }
#endif
        
        self.splashScreen = [[UIImageView alloc] initWithFrame: self.view.bounds];
        self.splashScreen.image = splashImage;

        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
        
        spinner.center = CGPointMake(0.5 * self.splashScreen.frame.size.width, spinnerFactor * self.splashScreen.frame.size.height);
        [spinner startAnimating];
        [self.splashScreen addSubview: spinner];
        
        [self.view addSubview: self.splashScreen];
        
        debug(@"setting timer to %f secs", kTimerTimeout);
        self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval: kTimerTimeout
                                                             target: self
                                                           selector: @selector(timeoutTimerFired:)
                                                           userInfo: nil
                                                            repeats: NO];
    }
    
#endif
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"intro_screen_viewed"]) {
        [AppDelegate showIntroView:self];
    }
    
    self.animationImageView.image = self.imagesArray[0];
    [NSTimer scheduledTimerWithTimeInterval:5
                                     target:self
                                   selector:@selector(changeImage:)
                                   userInfo:nil
                                    repeats:YES];

}





- (void)viewDidUnload
{
    [super viewDidUnload];
    debug(@"START: did unload");
    
    self.infoButton = nil;
    self.coolStuffButton = nil;
    self.takePictureButton = nil;
    self.openLibraryButton = nil;
}


- (void)viewWillAppear: (BOOL)animated
{
    [super viewWillAppear: animated];
    [[UIApplication sharedApplication] setStatusBarHidden: YES withAnimation: UIStatusBarAnimationNone];
    
    if (_fromFbFriendsView)
    {
        _fromFbFriendsView=NO;
        [self facebookViewControllerDoneWasPressed];
        
    }
    //[[Tapdaq sharedSession] showInterstitial];
}


- (void)viewDidAppear: (BOOL)animated
{
    [super viewDidAppear: animated];
//    [self animateImages];

}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (self.showingFriendPicker) {
        [self addSearchBarToFriendPickerView];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)closeModalViews: (NSNotification*)info
{
    if ( nil != self.presentedViewController &&
        [self.presentedViewController isKindOfClass: [UIImagePickerController class]] ) {
        
        [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    }
}


- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark - Public

- (void)timeoutTimerFired:(NSTimer*)theTimer
{
    if ( self.timeoutTimer == theTimer ) {
        debug(@"timeoutTimer timer fired");
        [self hideSplash];
    }
    else {
        error(@"unknown timer fired");
    }
}


- (void)hideSplash
{
    if ( nil != self.timeoutTimer ) {
        debug(@"invalidating timer");
        [self.timeoutTimer invalidate];
        self.timeoutTimer = nil;
    }
    
    if ( nil != self.splashScreen ) {
        debug(@"hiding Splash");
        [self.splashScreen removeFromSuperview];
        self.splashScreen = nil;
    }
    
    
    //
    // Cancel advertisement is it wasn't showed yet
    //
    [ALInterstitialAd unscheduleCurrentAdvertisement];
}


#pragma mark - FB Layout

- (void)addSearchBarToFriendPickerView
{
//    if (self.searchBar == nil) {
//        CGFloat searchBarHeight = 44.0;
//        self.searchBar =
//        [[UISearchBar alloc]
//         initWithFrame:
//         CGRectMake(0,50,
//                    self.view.bounds.size.width,
//                    searchBarHeight)];
//        self.searchBar.autoresizingMask = self.searchBar.autoresizingMask |
//        UIViewAutoresizingFlexibleWidth;
//        self.searchBar.delegate = self;
//        self.searchBar.showsCancelButton = YES;
//        
//        [self.friendPickerController.canvasView addSubview:self.searchBar];
//        CGRect newFrame = self.friendPickerController.view.bounds;
//        newFrame.size.height -= searchBarHeight;
//        newFrame.origin.y = searchBarHeight;
//        self.friendPickerController.tableView.frame = newFrame;
//    }
}

#pragma mark - Actions

- (void)goInfo: (id)sender
{
    debug(@"INFO pressed");
    
    InfoViewController *infoViewController = [[InfoViewController alloc] initWithNibName: nil bundle: nil];
    infoViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    NavController *navController = [[NavController alloc] initWithRootViewController: infoViewController];
    navController.navigationBarHidden = YES;
    
    
    //[self.navigationController presentModalViewController: navController animated: YES];
    [self.navigationController presentViewController: navController animated: YES completion:nil];
}


- (void)goCoolStuff: (id)sender
{
//    debug(@"COOL STUFF pressed");
//    [Flurry logEvent: @"OpenCoolStuff"];
//    
//    if( [FacebookManager sharedInstance].isFacebookReachable ) {
//        
////        [FlurryAppCircle openCatalog: @"COOL_STUFF_CATALOG_HOOK"
////                   canvasOrientation: @"portrait"
////                      canvasAnimated: YES];
////        [FlurryAppCircle
////         openTakeover:@"COOL_STUFF_CATALOG_HOOK"
////         orientation:@"portrait"
////         rewardImage:nil
////         rewardMessage:nil
////         userCookies:nil];
//    }
//    else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString( @"Error", @"" )
//                                                        message: NSLocalizedString( @"Internet connection required to see More apps", @"Alert title" )
//                                                       delegate: nil
//                                              cancelButtonTitle: NSLocalizedString( @"Dismiss", @"")
//                                              otherButtonTitles: nil];
//        [alert show];
//    }
    
    WebViewController *legalController = [[WebViewController alloc] initWithNibName: nil bundle: nil];
    legalController.title = @"More Info";
    legalController.url = [NSURL URLWithString: @"http://www.crearapp.com/hairstyles-and-make-up/"];
    legalController.delegate = self;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController: legalController];
    [self presentViewController: navController animated: YES completion:nil];
}

- (void)cancelWebViewController: (id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)goToStache: (id)sender
{
    PictureViewController *pictureViewController = [[PictureViewController alloc] initWithNibName: nil bundle: nil];
    pictureViewController.sourceImage = [UIImage imageNamed: @"sri_kobzar.jpg"];
    [self.navigationController pushViewController: pictureViewController animated: YES];
}


#pragma mark - Pictures taking

- (void)pickImageFromLibrary: (id)sender
{
    [Flurry logEvent: @"PickImage"];

    [self openImagePickerWithSourceType: UIImagePickerControllerSourceTypePhotoLibrary];

}


- (void)takePicture: (id)sender
{
    [Flurry logEvent: @"TakeImage"];
    
    [self openImagePickerWithSourceType: UIImagePickerControllerSourceTypeCamera];
    
}


- (void)openImagePickerWithSourceType: (UIImagePickerControllerSourceType)sourceType
{
    if ( ![UIImagePickerController isSourceTypeAvailable: sourceType] ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString( @"Error", @"" )
                                                        message: NSLocalizedString( @"We are sorry, but this functionality is not available at your device.", @"No camera eror" )
                                                       delegate: nil
                                              cancelButtonTitle: NSLocalizedString( @"Dismiss", @"")
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = sourceType;
    self.isCameraShown = YES;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
       self.popover = [[UIPopoverController alloc] initWithContentViewController:(UIViewController *)picker];
        CGRect takePhotoRect;
        takePhotoRect.origin = self.view.frame.origin;
        takePhotoRect.size.width = 1;
        takePhotoRect.size.height = 1;
        [self.popover setPopoverContentSize:CGSizeMake(320.0, 216.0)];
                
        [self.popover presentPopoverFromRect:takePhotoRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else{
        [self presentViewController:picker animated:YES completion:nil ];
    }

    
}
- (void) bannerTapped: (id) sender
{
//    PurchasePopupVC* popupVC = [[PurchasePopupVC alloc]initWithNibName:@"PurchasePopupVC" bundle:nil];
//    popupVC.delegate = self;
//    popupVC.packName=@"Main";
//    [self presentViewController:popupVC animated:YES completion:nil];

}

- (void)facebookViewControllerDoneWasPressed
{
        [Flurry logEvent: @"PickedFBFriendImage"];
    NSLog(@"%@",self.selectedArray);
    
        for (NSMutableDictionary* user in self.selectedArray)
        {
            
            self.hud = [MBProgressHUD showHUDAddedTo: self.view animated: YES];
            self.hud.delegate = self;
            self.hud.labelText = NSLocalizedString(@"Getting your friend's pic…", @"HUD title");
            
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: [NSString stringWithFormat: @"https://graph.facebook.com/%@/picture?width=640&height=640", [user objectForKey:@"id"]]]];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                       if ( !error )
                                       {
                                           [DataModel sharedInstance].currentFBFriend = user;
                                           if ([data length] > 0){
                                               UIImage *pickedImage = [[UIImage alloc] initWithData:data];
                                               [MBProgressHUD hideHUDForView: self.view animated: YES];
                                               PictureViewController *pictureViewController = [[PictureViewController alloc] initWithNibName: nil bundle: nil];
                                               pictureViewController.sourceImage = pickedImage;
                                               pictureViewController.fromView = @"FB";
                                               [self.navigationController pushViewController: pictureViewController animated: YES];
                                           }
                                           else
                                           {
                                               [MBProgressHUD hideHUDForView: self.view animated: YES];
                                               
                                               UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"")
                                                                                                   message: NSLocalizedString(@"Can't get your friend pic", @"")
                                                                                                  delegate: nil
                                                                                         cancelButtonTitle: NSLocalizedString(@"Dismiss", @"")
                                                                                         otherButtonTitles: nil, nil];
                                               [alertView show];
                                               
                                               
                                               
                                           }
                                           NSLog(@"downloaded FULL size %lu",(unsigned long)data.length);
                                       } else
                                       {
                                           [MBProgressHUD hideHUDForView: self.view animated: YES];
                                           
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"")
                                                                                               message: NSLocalizedString(@"Can't get your friend pic", @"")
                                                                                              delegate: nil
                                                                                     cancelButtonTitle: NSLocalizedString(@"Dismiss", @"")
                                                                                     otherButtonTitles: nil, nil];
                                           [alertView show];

                                       }
                                   }];
        }
    }

- (void) getFBPicture: (id) sender
{
    FBFriendsCustomViewController *fBFriendsCustomViewController = [[FBFriendsCustomViewController alloc] init];
    fBFriendsCustomViewController.multipleSelection=NO;
    fBFriendsCustomViewController.viewName=@"StartPageViewController";
    [self.navigationController pushViewController: fBFriendsCustomViewController animated: YES];
    
    
//    [Flurry logEvent: @"PickFBFriendImage"];
//
//    if ( ![[FacebookManager sharedInstance] isFacebookReachable] ) {
//        error(@"no route to Facebook - cannot share");
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error" , @"")
//                                                        message: NSLocalizedString(@"You need to be connected to Internet to interact with Facebook.", @"Info screen - share facebook - no connection error alert text")
//                                                       delegate: nil
//                                              cancelButtonTitle: NSLocalizedString(@"Dismiss", @"")
//                                              otherButtonTitles: nil];
//        [alert show];
//        return;
//    }
//    
//    debug(@"sharing to facebook");
//    if ( [[FacebookManager sharedInstance] isLoggedIn] ) {
//        
//        [self.navigationController setNavigationBarHidden:NO animated:NO];
//
//        if (self.friendPickerController == nil) {
//            // Create friend picker, and get data loaded into it.
//            self.friendPickerController = [[FBFriendPickerViewController alloc] init];
//            self.friendPickerController.title = @"Pick Friends";
//            self.friendPickerController.delegate = self;
//            self.friendPickerController.allowsMultipleSelection = NO;
//            
//        }
//        
//        [self.friendPickerController loadData];
//        [self.friendPickerController clearSelection];
//        self.showingFriendPicker = YES;
////        [self.friendPickerController presentViewController:self animated:YES completion:^{
////            
////        }];
//        [self.navigationController pushViewController:self.friendPickerController animated:YES];
//
////        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:self.friendPickerController];
////        [self presentViewController:nc animated:YES completion:nil];
//
////        [self.friendPickerController
////         presentModallyFromViewController:self
////         animated:YES
////         handler:^(FBViewController *sender, BOOL donePressed) {
////             [self addSearchBarToFriendPickerView];
////             if (donePressed) {
////                 [Flurry logEvent: @"PickedFBFriendImage"];
////                 
////                 for (id<FBGraphUser> user in self.friendPickerController.selection) {
////                     
////                     self.hud = [MBProgressHUD showHUDAddedTo: self.view animated: YES];
////                     self.hud.delegate = self;
////                     self.hud.labelText = NSLocalizedString(@"Getting your friend's pic…", @"HUD title");
////                     
////                     
////                     __unsafe_unretained __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL: [NSURL URLWithString: [NSString stringWithFormat: @"https://graph.facebook.com/%@/picture?width=640&height=640", [user objectForKey:@"id"]]]];
////                     [request setCompletionBlock: ^{
////                         
////                         [DataModel sharedInstance].currentFBFriend = user;
////                         
////                         debug(@"Get Picture request completed");
////                         debug(@"data len: %d", [[request responseData] length]);
////                         if ([[request responseData] length] > 0){
////                             UIImage *pickedImage = [[UIImage alloc] initWithData:[request responseData]];
////                             [MBProgressHUD hideHUDForView: self.view animated: YES];
////                             PictureViewController *pictureViewController = [[PictureViewController alloc] initWithNibName: nil bundle: nil];
////                             pictureViewController.sourceImage = pickedImage;
////                             [self.navigationController pushViewController: pictureViewController animated: YES];
////                         }
////                         else
////                         {
////                             [MBProgressHUD hideHUDForView: self.view animated: YES];
////                             
////                             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"")
////                                                                                 message: NSLocalizedString(@"Can't get your friend pic", @"")
////                                                                                delegate: nil
////                                                                       cancelButtonTitle: NSLocalizedString(@"Dismiss", @"")
////                                                                       otherButtonTitles: nil, nil];
////                             [alertView show];
////                             
////                             
////                             
////                         }
////                     }];
////                     
////                     [request setFailedBlock: ^{
////                         [MBProgressHUD hideHUDForView: self.view animated: YES];
////                         
////                         debug(@"Get Picture request  failed with error: %@", request.error);
////                         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"")
////                                                                             message: NSLocalizedString(@"Can't get your friend pic", @"")
////                                                                            delegate: nil
////                                                                   cancelButtonTitle: NSLocalizedString(@"Dismiss", @"")
////                                                                   otherButtonTitles: nil, nil];
////                         [alertView show];
////                         
////                     }];
////                     
////                     [request setStartedBlock: ^{
////                         debug(@"Get Picture request started");
////                     }];
////                     
////                     [request startAsynchronous];
////                     break;
////                 }
////             }
////         }];
//    }
//    else {
//        debug(@"share to facebook - initiating login");
//        _currentAPICall = kAPIFriendsForDialogRequests;
//        
//        [FacebookManager sharedInstance].loginDelegate = self;
//        [[FacebookManager sharedInstance] logIn];
//    }
}


- (void) handleSearch:(UISearchBar *)searchBar {
//    [searchBar resignFirstResponder];
//    self.searchText = searchBar.text;
//    [self.friendPickerController updateView];
}


//- (BOOL)friendPickerViewController:(FBFriendPickerViewController *)friendPicker
//                 shouldIncludeUser:(id<FBGraphUser>)user
//{
//    if (self.searchText && ![self.searchText isEqualToString:@""]) {
//        NSRange result = [user.name
//                          rangeOfString:self.searchText
//                          options:NSCaseInsensitiveSearch];
//        if (result.location != NSNotFound) {
//            return YES;
//        } else {
//            return NO;
//        }
//    } else {
//        return YES;
//    }
//    return YES;
//}



#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController: (UIImagePickerController*)picker didFinishPickingMediaWithInfo: (NSDictionary*)info
{
    //[picker dismissModalViewControllerAnimated: YES];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.popover dismissPopoverAnimated:YES];
    }
    else
    {
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    
    
    self.isCameraShown = NO;
    
    UIImage *pickedImage = [info objectForKey: @"UIImagePickerControllerOriginalImage"];
    
    PictureViewController *pictureViewController = [[PictureViewController alloc] initWithNibName: nil bundle: nil];
    pictureViewController.sourceImage = pickedImage;
    [self.navigationController pushViewController: pictureViewController animated: YES];
}


#pragma mark - MBProgressHUDDelegate

- (void)hudWasHidden:(MBProgressHUD *)hud
{
	[hud removeFromSuperview];
	self.hud = nil;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchText = searchBar.text;
//    [self.friendPickerController updateView];
}

- (void)searchBarSearchButtonClicked:(UISearchBar*)searchBar
{
    [self handleSearch:searchBar];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    self.searchText = nil;
    [searchBar resignFirstResponder];
//    [self.friendPickerController updateView];
}


- (void)facebookViewControllerCancelWasPressed:(id)sender {
    self.showingFriendPicker = NO;
}


//- (void)facebookViewControllerDoneWasPressed:(id)sender {
//    self.showingFriendPicker = NO;
//}


#pragma mark - FacebookManagerLoginDelegate

- (void)facebookDidLogIn
{
    debug(@"did LOG IN");
    [self getFBPicture: nil];
}


- (void)facebookDidNotLogin: (BOOL)cancelled;
{
    
    
    if ( !cancelled ) {
        float currentVersion = 6.0;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= currentVersion)
        {
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Warning", @"")
                                                                     message: NSLocalizedString(@"To use Facebook you must allow the app in Settings->Facebook", @"Facebook authorization failure - alert text")
                                                                    delegate: nil
                                                           cancelButtonTitle: NSLocalizedString(@"Dismiss", @"")
                                                           otherButtonTitles: nil];
            [errorAlertView show];
            
        }
    }
}


- (void)facebookDidLogOut
{
    debug(@"did LOG OUT");
}

-(void)unlockEverything
{
    [self dismissViewControllerAnimated:NO completion:nil];
    [self unlockAllPressedFromCurtainView:nil];
}

- (void)unlockAllPressedFromCurtainView: (id)curtainView
{
    [MBProgressHUD showHUDAddedTo: self.view animated: YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[DataModel sharedInstance] purchaseUnlockAllMustaches:self.view];
    });
}

#pragma mark - Subviews

- (UIImageView *)animationImageView
{
    if (!_animationImageView)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            _animationImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, [GUIHelper getBottomYForView:self.coolStuffButton]+20,300, 300)];
        }
        else
        {
            if ( [GUIHelper isPhone4] ) {
                _animationImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-180)/2, [GUIHelper getBottomYForView:self.coolStuffButton]+5,180, 180)];
            }else{
                _animationImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-180)/2, [GUIHelper getBottomYForView:self.coolStuffButton]+20,180, 180)];

            }
        }

        

    }
    
    return _animationImageView;
}


#pragma mark - Getters

- (NSArray *)imagesArray
{
    if (!_imagesArray)
    {
        _imagesArray = [self createImagesArray];
    }
    
    return _imagesArray;
}

- (CADisplayLink *)displayLink
{
    if (!_displayLink)
    {
        _displayLink = [CADisplayLink displayLinkWithTarget:self
                                                   selector:@selector(linkProgress)];
    }
    
    return _displayLink;
}
- (NSArray *)createImagesArray
{
    NSArray *animationArray=[NSArray arrayWithObjects:
                             [UIImage imageNamed:@"image.png"],
                             [UIImage imageNamed:@"image_1.png"],
                             [UIImage imageNamed:@"image_2.png"],
                             [UIImage imageNamed:@"image_3.png"],
                             [UIImage imageNamed:@"image_4.png"],
                             nil];
    return animationArray;
}

#pragma mark - Animation

- (void)animateImages
{
    self.displayLink.frameInterval = 100;
    self.frameNumber = 0;
    [self.displayLink addToRunLoop:[NSRunLoop mainRunLoop]
                           forMode:NSRunLoopCommonModes];
}

- (void)linkProgress
{
    if (self.frameNumber > _imagesArray.count)
    {
        self.frameNumber=0;
    }
    
    [UIView transitionWithView:self.animationImageView duration:3 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
        self.animationImageView.image = self.imagesArray[self.frameNumber++];
        self.frameNumber++;
    } completion:nil];
}
-(void)changeImage:(NSTimer *)timer
{
    NSInteger currentIndex = [_imagesArray indexOfObject:self.animationImageView.image];
    NSInteger nextIndex    = (currentIndex + 1) % _imagesArray.count;
    
    [UIView transitionWithView:self.animationImageView duration:3 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
        self.animationImageView.image = self.imagesArray[nextIndex];
        self.frameNumber++;
    } completion:nil];

    
}



@end
