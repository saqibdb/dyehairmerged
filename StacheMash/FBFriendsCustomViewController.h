//
//  FBFriendsCustomViewController.h
//  YourPerfectHairstyle32
//
//  Created by Qaiser Abbas on 6/27/17.
//  Copyright © 2017 APPreciate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface FBFriendsCustomViewController : UIViewController
{
    IBOutlet UISearchBar *customsearchBar;
    NSMutableArray *dataSource;
    NSMutableArray *selectedMarks;

    IBOutlet UITableView *cutomTableView;
}

@property (nonatomic, assign) BOOL multipleSelection;
@property (strong, nonatomic) NSString *viewName;

- (IBAction)backButtonTap:(UIButton *)sender;
- (IBAction)doneButtonTap:(UIButton *)sender;
@end
