//
//  ApplicationRater.m
//  MustacheBash
//
//  Created by Alexander Gavrilko on 06/02/15.
//  Copyright (c) 2015 Bright Newt. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import "NSTimer(block).h"
#import "UIAlertView(block).h"
#import "MFMailComposeViewController(block).h"
#import "ApplicationRater.h"
#import "Flurry.h"
#import "UIDeviceHardware.h"

static const NSTimeInterval kPromtDelay                 = 90.00;

static NSString * const kUDActivationCountKey           = @"ApplicationRater.activationCount";
static NSString * const kUDFireNumbersKey               = @"ApplicationRater.fireNumbers";

@implementation ApplicationRater
{
    NSInteger _activationCount;
    NSMutableIndexSet *_fireNumbers;
    
    void(^_agreementHandler)();
    
    
    NSTimer *_promtTimer; // timer for delayed promt
    UIAlertView *_currentAlertView; // to prevent showing second alert on the screen
}

+ (BOOL) awakeWithAgreementHandler:(void (^)())agreementHandler
{
    static ApplicationRater *defaultRater;
    static dispatch_once_t onceToken;
    
    BOOL result = defaultRater == nil;
    
    dispatch_once(&onceToken, ^{
        
        defaultRater = [[ApplicationRater alloc] init];
        if ( defaultRater )
        {
            defaultRater->_agreementHandler = [agreementHandler copy];
            [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
 
                [defaultRater _increaseActivationCount];
                [defaultRater _save];
                
            }];
        }
        
    });
    
    return result;
}

- (instancetype) init
{
    self = [super init];
    
    if ( self )
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        _activationCount = [userDefaults integerForKey:kUDActivationCountKey];
        _fireNumbers = [[NSMutableIndexSet alloc] init];
        
        NSArray *numberArray = [userDefaults arrayForKey:kUDFireNumbersKey];
        if ( numberArray )
        {
            for ( NSNumber *number in numberArray )
            {
                [_fireNumbers addIndex:number.integerValue];
            }
        }
        //
        // If there is no predefined list in user defaults we should add it
        //
        else
        {
            NSInteger fireNumbers[] = {
                APPLICATION_RATER_FIRE_NUMBER,
                -1,
            };
            
            NSMutableArray *numberArray = [[NSMutableArray alloc] init];
            for ( NSInteger i = 0; fireNumbers[i] > 0; ++i )
            {
                NSInteger numberValue = fireNumbers[i];
                
                [numberArray addObject:@(numberValue)];
                [_fireNumbers addIndex:numberValue];
            }
            
            [userDefaults setObject:numberArray forKey:kUDFireNumbersKey];
            [userDefaults synchronize];
        }
    }
    
    return _fireNumbers.count ? self : nil;
}

#pragma mark - private

- (BOOL) _increaseActivationCount
{
    NSLog(@"@'Trace JR _increaseActivationCount");
    
    if ( _currentAlertView )
    {
        return NO;
    }
    //JRMB date -----------------------------
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *currentDate = [NSDate date];
    NSString *currDateString = [formatter stringFromDate:currentDate];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date1 = [dateFormatter dateFromString:currDateString];
    
    NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
    [dateFormatte setDateFormat:@"yyyy-MM-dd"];
    NSDate *date2 = [dateFormatte dateFromString:@"2017-11-22"];//2017-07-30
    
    unsigned int unitFlags = NSDayCalendarUnit;
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:date1  toDate:date2  options:0];
    
    
    int ddd = [comps day];
   
    
    //----------------------------------------------
    if (ddd <= 0)   if ( [_fireNumbers containsIndex:++_activationCount] )
    //if ( [_fireNumbers containsIndex:++_activationCount] )
    {
         NSLog(@"@'Trace JR Inside date activation");
        if ( !_promtTimer )
        {
            _promtTimer = [NSTimer scheduledTimerWithTimeInterval:kPromtDelay block:^(NSTimer *sender) {
                
                [_promtTimer setFireDate:[NSDate distantFuture]];
                _currentAlertView = [[UIAlertView alloc] initWithTitle:nil
                                                               message:NSLocalizedString(@"Are you happy with this app?", @"iRate alert text")
                                               buttonTitlesAndHandlers:
                                     
                                     NSLocalizedString(@"No :(", @"iRate alert - NO"), ^{
                                         
                                         if ( [MFMailComposeViewController canSendMail] )
                                         {
                                             _currentAlertView = [[UIAlertView alloc] initWithTitle:nil
                                                                                            message:NSLocalizedString(@"Do you want to send the feedback to the customer service?", nil)
                                                                            buttonTitlesAndHandlers:
                                                                  
                                                                  NSLocalizedString(@"No :(", nil), ^{
                                                                      _currentAlertView = nil;
                                                                  },
                                                                  
                                                                  NSLocalizedString(@"Yes!", nil), ^{
                                                                      _currentAlertView = nil;
                                                                      [self _showFeedbackMailViewController];
                                                                  },
                                                                  
                                                                  nil];
                                             [_currentAlertView show];
                                         }
                                         else
                                         {
                                             _currentAlertView = nil;
                                         }
                                         
                                         [Flurry logEvent: @"iRateNO"];
                                     },
                                     
                                     NSLocalizedString(@"Yes!", @"iRate alert - YES"), ^{
                                         
                                         _currentAlertView = [[UIAlertView alloc] initWithTitle:nil
                                                                                        message:NSLocalizedString(@"Great you like it! If you review the app you will get a package unlocked.", nil)
                                                                        buttonTitlesAndHandlers:
                                                     
                                                              NSLocalizedString(@"Not interested", nil), ^{
                                                                  _currentAlertView = nil;
                                                              },
                                                              
                                                              NSLocalizedString(@"Yes, of course", nil), ^{
                                                                  
                                                                  _currentAlertView = nil;
                                                                  
                                                                  NSURL *url = [NSURL URLWithString: @"https://itunes.apple.com/us/app/hair-dye-wig-color-changer/id1031041753?mt=8"];
                                                                  [[UIApplication sharedApplication] openURL:url];
                                                                  
                                                                  if ( _agreementHandler )
                                                                  {
                                                                      _agreementHandler();
                                                                      _agreementHandler = nil;
                                                                  }
                                                                  
                                                                  [_fireNumbers removeAllIndexes];
                                                                  [self _save];
                                                                  
                                                                  [Flurry logEvent: @"iRateYES"];
                                                              },
                                                    
                                                              nil];
                                         [_currentAlertView show];
                                     },
                                     
                                     nil];
                [_currentAlertView show];
                
            } userInfo:nil repeats:YES];
        }
        else
        {
            [_promtTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:kPromtDelay]];
        }

        [_fireNumbers removeIndex:_activationCount];
    }
    
    return YES;
}

- (void) _save
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *numberArray = [[NSMutableArray alloc] initWithCapacity:_fireNumbers.count];
    [_fireNumbers enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [numberArray addObject:@(idx)];
    }];
    
    [userDefaults setInteger:_activationCount forKey:kUDActivationCountKey];
    [userDefaults setObject:numberArray forKey:kUDFireNumbersKey];
    [userDefaults synchronize];
}

- (void) _showFeedbackMailViewController
{
    // get device properties
    UIDevice *device = [UIDevice currentDevice];
    UIDeviceHardware *hardware = [[UIDeviceHardware alloc] init];
    
    NSMutableString *deviceProperties = [[NSMutableString alloc] init];
    [deviceProperties appendFormat: NSLocalizedString(@"device: %@\n", @""), [hardware platformString]];
    [deviceProperties appendFormat: NSLocalizedString(@"system: %@ %@\n", @""), device.systemName, device.systemVersion];
    
    // create email composer
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    [controller setMailComposeControllerDidFinishWithResult:^(MFMailComposeViewController *controller, MFMailComposeResult result, NSError *error) {
        
        switch ( result )
        {
            case MFMailComposeResultCancelled:
            {
                [Flurry logEvent: @"iRateEmailCancelled"];
                break;
            }
                
            case MFMailComposeResultFailed:
            {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error" , @"")
                                            message:[NSString stringWithFormat: NSLocalizedString(@"Error sending email: %@", @""), [error localizedDescription]]
                            buttonTitlesAndHandlers:
                  NSLocalizedString(@"Dismiss", @""), nil,
                  nil] show];

                [Flurry logEvent: @"iRateEmailFailed"];
                break;
            }
                
            case MFMailComposeResultSaved:
            {
                [Flurry logEvent: @"iRateEmailSaved"];
                break;
            }
                
            case MFMailComposeResultSent:
            {
                [Flurry logEvent: @"iRateEmailSent"];
                break;
            }
        }
        
        [controller dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [controller setToRecipients:[NSArray arrayWithObject:@"soporte1@crearapp.com"]];
    
    [controller setSubject:
     [NSString stringWithFormat: NSLocalizedString(@"Your app Hair dye makes me sad - ver %@", @""),
      [[[NSBundle mainBundle] infoDictionary] objectForKey: @"CFBundleVersion"]]];
    
    [controller setMessageBody:
     [NSString stringWithFormat: NSLocalizedString(@"Hi there,\n\n Your app is frustrating to use because...\n\n\n\n\nMy device properties are:\n%@", @""), deviceProperties]
                        isHTML: NO];
    
    // show email composer
    [[[UIApplication sharedApplication] delegate].window.rootViewController presentViewController:controller animated:YES completion:nil];
}

@end
