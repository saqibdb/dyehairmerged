//
//  PictureViewController.h
//  StacheMash
//
//  Created by Konstantin Sokolinskyi on 1/18/12.
//  Copyright (c) 2012 Bright Newt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import <Revmob/Revmob.h>
#import <Revmob/Revmob.h>

#import "BaseViewController.h"
#import "MustacheCurtainView.h"
#import "StacheView.h"
#import "DataModel.h"
#import "MBProgressHUD.h"

@interface PictureViewController : BaseViewController
    <UINavigationControllerDelegate,
    UIGestureRecognizerDelegate,
    MustacheCurtainViewDelegate,
    StacheViewDelegate,
    UIAlertViewDelegate,
    DataModelPurchaseDelegate,
    RevmobDelegate,
    MBProgressHUDDelegate>

@property (strong, nonatomic) UIImage *sourceImage;
// Sun
@property (strong, nonatomic) NSString *fromView;

//@property (strong, nonatomic) UIImage *originaImage;


@end
