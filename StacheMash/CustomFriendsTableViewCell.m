//
//  CustomFriendsTableViewCell.m
//  YourPerfectHairstyle32
//
//  Created by Qaiser Abbas on 6/29/17.
//  Copyright © 2017 APPreciate. All rights reserved.
//

#import "CustomFriendsTableViewCell.h"

@implementation CustomFriendsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
