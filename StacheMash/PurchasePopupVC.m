//
//  PurchasePopupVC.m
//  Change Your Hair 2
//
//  Created by Sreekanthb on 03/05/2016.
//  Copyright © 2016 APPreciate. All rights reserved.
//

#import "PurchasePopupVC.h"
#import "GUIHelper.h"
#import "DMPack.h"
#import "DataModel.h"

@interface PurchasePopupVC ()

@end

@implementation PurchasePopupVC

@synthesize bgImage;

-(IBAction)dismissSelf
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    if ([self.packName isEqualToString:@"Celebrities"])
//    {
//        if ([GUIHelper isIpad]) {
//            if ([GUIHelper isIPadretina]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner2732x2048-ce.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner1536x2048-ce.jpg"];
//            }
//        } else {
//            if ([GUIHelper isPhone5]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x1136-ce.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x960-ce.jpg"];
//            }
//        }
//
//    }
//    else if ([self.packName isEqualToString:@"Alternative"])
//    {
//        if ([GUIHelper isIpad]) {
//            if ([GUIHelper isIPadretina]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner2732x2048-al.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner1536x2048-al.jpg"];
//            }
//        } else {
//            if ([GUIHelper isPhone5]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x1136-al.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x960-al.jpg"];
//            }
//        }
//
//    }
//    else if ([self.packName isEqualToString:@"Most Popular"])
//    {
//        if ([GUIHelper isIpad]) {
//            if ([GUIHelper isIPadretina]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner2732x2048-mo.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner1536x2048-mo.jpg"];
//            }
//        } else {
//            if ([GUIHelper isPhone5]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x1136-mo.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x960-mo.jpg"];
//            }
//        }
// 
//    }
//    else if ([self.packName isEqualToString:@"Latest"])
//    {
//        if ([GUIHelper isIpad]) {
//            if ([GUIHelper isIPadretina]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner2732x2048-la.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner1536x2048-la.jpg"];
//            }
//        } else {
//            if ([GUIHelper isPhone5]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x1136-la.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x960-la.jpg"];
//            }
//        }
//  
//    }
//    else if ([self.packName isEqualToString:@"Main"])
//    {
//        if ([GUIHelper isIpad]) {
//            if ([GUIHelper isIPadretina]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner1536x2048.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner2732x2048.jpg"];
//            }
//        } else {
//            if ([GUIHelper isPhone5]) {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x1136.jpg"];
//            } else {
//                self.bgImage.image = [UIImage imageNamed:@"banner640x960.jpg"];
//            }
//        }
//        
//    }

    if ([GUIHelper isIpad]) {
        if ([GUIHelper isIPadretina]) {
            self.bgImage.image = [UIImage imageNamed:@"banner_2732.jpg"];
        } else {
            self.bgImage.image = [UIImage imageNamed:@"banner_2048.jpg"];
        }
    } else {
        if ([GUIHelper isPhone5]) {
            self.bgImage.image = [UIImage imageNamed:@"banner_1136.jpg"];
        } else {
            self.bgImage.image = [UIImage imageNamed:@"banner_960.jpg"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)removeBanner
{
    [self.delegate removeBanner];
}


-(IBAction)unlockPack
{
    
    if ([self.packName isEqualToString:@"Main"])
    {
        [self.delegate unlockEverything];
    }
    else
    {
        for (DMPack *pack in [DataModel sharedInstance].packsArray)
        {
            if ([pack.name isEqualToString:[NSString stringWithFormat:@"%@",self.packName]])
            {
                [self.delegate unlockPack:pack];
                break;
            }
        }
    }
}


-(IBAction)unlockEverything
{
    if ([self.packName isEqualToString:@"Main"])
    {
        return;
    }

    [self.delegate unlockEverything];
}

@end
