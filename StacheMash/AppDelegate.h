//
//  AppDelegate.h
//  StacheMash
//
//  Created by Konstantin Sokolinskyi on 1/17/12.
//  Copyright (c) 2012 Bright Newt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Revmob/Revmob.h>
//#import <RevMobAds/RevMobAdsDelegate.h>
#import <Tapdaq/Tapdaq.h>

// IDs for Advert SDKs
// Mustache Bash area

//Flurry startSession
#define FLURRY_SESSION_ID @"PK7RHN7DTFFCM9XBK5NF"

//rate.appStoreID
#define APPSTORE_RATE_ID 1031041753

#define APP_ID @"1031041753"

//RevMobFullscreen
//#define REVMOB_FULLSCREEN_PLACEMENT_ID @"5481c4478ce6f90409396bf8"

//revMobBannerView
// #define  REVMOB_BANNER_ID @"55e70025497d27e0664df1b8"

#define  REVMOB_BANNER_ID @"55e6ff8e497d27e0664df1ac"

//postcard id
#define SINCELERY_ID @"L1GYO8345BUIAET849Y7N5NVWKZNZRBEN29AD1FO"

// Facebook app id
#define FACEBOOK_APP_ID @"1657989987778005"//@"238923342858696"

#define INSTAGRAM_CAPTION @"@jordicrack1 #your-perfect-hairstyle"



#define NAG_SCREENS_ON 1

@interface AppDelegate : UIResponder
    <UIApplicationDelegate,
    RevmobDelegate,
    TapdaqDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (copy, nonatomic) void(^resumeHandler)(AppDelegate *sender);

- (void)openReferralURL:(NSURL *)referralURL;

+(void)showIntroView:(UIViewController*)parentView;

@end
